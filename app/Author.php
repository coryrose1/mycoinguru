<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Post;

class Author extends Model
{
    protected $table   = 'post_author';

    protected $guarded = ['id'];

    public function posts()
    {
        return $this->hasMany(Post::class);
    }
}