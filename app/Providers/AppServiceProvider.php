<?php

namespace App\Providers;

use App\Oauth\CoinbaseProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\ServiceProvider;
use Laravel\Horizon\Horizon;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        // Coinbase Socialiate OAuth
        $this->bootCoinbaseSocialite();

        Blade::if('writer', function() {
            if (Auth::user()->role_id > 1)
            {
                return true;
            }
            return false;
        });
        Blade::if('admin', function() {
            if (Auth::user()->role_id > 2)
            {
                return true;
            }
            return false;
        });
        Blade::if('darkmode', function() {
            if (Auth::check())
            {
                if (Auth::user()->dark_mode == 1)
                {
                    return true;
                }
                return false;
            }
        });

        if ($this->app->environment() !== 'production') {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }

        Horizon::auth(function ($request) {
            return $this->app->environment('local') || ($request->user() && $request->user()->id === 1);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    private function bootCoinbaseSocialite()
    {
        $socialite = $this->app->make('Laravel\Socialite\Contracts\Factory');
        $socialite->extend(
            'coinbase',
            function ($app) use ($socialite) {
                $config = $app['config']['services.coinbase'];
                return $socialite->buildProvider(CoinbaseProvider::class, $config);
            }
        );
    }
}
