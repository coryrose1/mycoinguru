<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GdaxImport extends Model
{
    protected $table = 'gdax_imports';

    protected $fillable = ['id', 'gdax_trade_id', 'gdax_order_id', 'user_id', 'coin_1', 'coin_2', 'action', 'trade_action', 'amount', 'usd', 'fees_usd', 'traded_amount', 'received_amount', 'date', 'imported', 'transaction_id', 'trade_id', 'sale_id'];

    protected $casts = [
        'amount' => 'float',
        'usd' => 'float',
        'fees_usd' => 'float',
    ];
}
