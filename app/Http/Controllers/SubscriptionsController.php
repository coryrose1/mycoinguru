<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SubscriptionsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $request)
    {
        $user = Auth::user();
        $stripeToken = $request->stripeToken;
        $user->newSubscription('Monthly', 'monthly')->create($stripeToken, [
            'email' => $user->email,
        ]);
    }
}
