<?php

namespace App\Http\Controllers;

use App\GdaxImport;
use App\Holdings;
use App\Jobs\FixAverageTradeCosts;
use App\Jobs\FixAverageSaleCosts;
use Illuminate\Http\Request;
use App\Trade;
use Auth;
use App\Transaction;
use App\Coin;
use Carbon\Carbon;
use Ixudra\Curl\Facades\Curl;

class TradesController extends Controller
{

    public function destroy($id)
    {
        $trade = Trade::with('transactions')->find($id);
        $transaction = $trade->transactions->last();
        FixAverageTradeCosts::dispatch($transaction)->delay(Carbon::now()->addSeconds(2));
        FixAverageSaleCosts::dispatch($transaction->user_id, $transaction->coin_symbol, $transaction->date)->delay(Carbon::now()->addSeconds(4));
        if ($trade->gdaxImport)
        {
            $import = GdaxImport::find($trade->gdaxImport->id);
            $import->imported = 0;
            $import->trade_id = null;
            $import->save();
        }
        if ($trade->binanceImport)
        {
            $import = GdaxImport::find($trade->binanceImport->id);
            $import->imported = 0;
            $import->trade_id = null;
            $import->save();
        }
        $trade->delete();
        return response()->json('deleted');
    }

    public function btcToCoin(Request $request)
    {
        $this->validate($request, [
            'tradedCoin' => 'required',
            'tradedAmount' => 'required',
            'receivedCoin' => 'required',
            'receivedAmount' => 'required',
            'btcRatio' => 'required',
            'date' => 'required',
            'hour' => 'required',
            'minute' => 'required',
            'amPm' => 'required',
        ]);
        // Get User
        $user = Auth::user();
        // Parse and format datetime -> GMT
        $date = Carbon::parse($request->date);
        $date = $date->toDateString();

        if ($request->amPm == 'PM' && $request->hour != 12)
        {
            $hour = $request->hour + 12;
        } else {
            $hour = $request->hour;
        }

        $parse = Carbon::parse($date.' '.$hour.':'.$request->minute.':00');
        $datetime = Carbon::createFromFormat('Y-m-d H:i:s', $parse, $user->timezone);
        $datetime = $datetime->timezone('GMT');
        // Time for DB
        $time_gmt = $datetime->toDateTimeString();

        // Get traded and received coin.
        $tradedCoin = Coin::where('symbol', '=', $request->tradedCoin)->first();
        $receivedCoin = Coin::where('symbol', '=', $request->receivedCoin)->first();
        // Get avg cost value for traded coin from Holdings
        $tradedAvgCost = $this->getAvgTradeCost($tradedCoin->symbol, $user->id, $time_gmt);
        // Get cost value at time of trade from API
        $start = $datetime->format('Y-m-d\TH:i:s\Z');
        $end = $datetime->addMinutes(10)->format('Y-m-d\TH:i:s\Z');
        $granularity = (int)60;

        $url = 'https://api.gdax.com/products/BTC-USD/candles/?start='.$start.'&end='.$end.'&granularity='.$granularity;
        $response = Curl::to($url)
            ->withContentType('application/json')
            ->withHeader('User-Agent: Test')
            ->returnResponseObject()
            ->get();

        $bitcoin_history = json_decode($response->content);
        $i = 0;
        $sum = 0;
        foreach ($bitcoin_history as $unit)
        {
            // Average the low and high price and add to sum
            $average = ($unit[1] + $unit[2])/2;
            $sum += $average;
            $i++;
        }
        $btc_historical_price = $sum/$i;


        $received_usd = $btc_historical_price * $request->btcRatio;
        $net_at_trade_usd = ($btc_historical_price * $request->tradedAmount) - ($tradedAvgCost * $request->tradedAmount);


        $trade = Trade::create([
            'user_id' => $user->id,
            'traded_coin' => $tradedCoin->symbol,
            'received_coin' => $receivedCoin->symbol,
            'traded_amount' => $request->tradedAmount,
            'received_amount' => $request->receivedAmount,
            'date' => $time_gmt,
            'btc_at_trade' => $btc_historical_price,
            'net_at_trade_usd' => $net_at_trade_usd,
            'btc_ratio' => $request->btcRatio
        ]);

        $traded = Transaction::create([
            'user_id' => $user->id,
            'coin_symbol' => 'BTC',
            'amount' => (-1 * $request->tradedAmount),
            'usd' => $tradedAvgCost,
            'trade_id' => $trade->id,
            'date' => $time_gmt
        ]);

        $received = Transaction::create([
            'user_id' => $user->id,
            'coin_symbol' => $receivedCoin->symbol,
            'amount' => $request->receivedAmount,
            'usd' => $received_usd,
            'trade_id' => $trade->id,
            'date' => $time_gmt
        ]);

        FixAverageTradeCosts::dispatch($received);
        FixAverageSaleCosts::dispatch($received->user_id, $received->coin_symbol, $received->date)->delay(Carbon::now()->addSeconds(2));

        return response()->json('success');

    }

    public function coinToBtc(Request $request)
    {
        $this->validate($request, [
            'tradedCoin' => 'required',
            'tradedAmount' => 'required',
            'receivedCoin' => 'required',
            'receivedAmount' => 'required',
            'btcRatio' => 'required',
            'date' => 'required',
            'hour' => 'required',
            'minute' => 'required',
            'amPm' => 'required',
        ]);
        // Get User
        $user = Auth::user();
        // Parse and format datetime -> GMT
        $date = Carbon::parse($request->date);
        $date = $date->toDateString();

        if ($request->amPm == 'PM' && $request->hour != 12)
        {
            $hour = $request->hour + 12;
        } else {
            $hour = $request->hour;
        }

        $parse = Carbon::parse($date.' '.$hour.':'.$request->minute.':00');
        $datetime = Carbon::createFromFormat('Y-m-d H:i:s', $parse, $user->timezone);
        $datetime = $datetime->timezone('GMT');
        $time_gmt = $datetime->toDateTimeString();

        // Get traded and received coin.
        $tradedCoin = Coin::where('symbol', '=', $request->tradedCoin)->first();
        $receivedCoin = Coin::where('symbol', '=', $request->receivedCoin)->first();
        // Get avg cost value for traded coin from Holdings
        $tradedAvgCost = $this->getAvgTradeCost($tradedCoin->symbol, $user->id, $time_gmt);
        // Get cost value at time of trade from API
        // Get cost value at time of trade from API
        $start = $datetime->format('Y-m-d\TH:i:s\Z');
        $end = $datetime->addMinutes(10)->format('Y-m-d\TH:i:s\Z');
        $granularity = (int)60;

        $url = 'https://api.gdax.com/products/BTC-USD/candles/?start='.$start.'&end='.$end.'&granularity='.$granularity;
        $response = Curl::to($url)
            ->withContentType('application/json')
            ->withHeader('User-Agent: Test')
            ->returnResponseObject()
            ->get();

        $bitcoin_history = json_decode($response->content);
        $i = 0;
        $sum = 0;
        foreach ($bitcoin_history as $unit)
        {
            // Average the low and high price and add to sum
            $average = ($unit[1] + $unit[2])/2;
            $sum += $average;
            $i++;
        }
        $btc_historical_price = $sum/$i;

        $traded_usd = $btc_historical_price * $request->btcRatio;
        $net_at_trade_usd = ($traded_usd * $request->tradedAmount) - ($tradedAvgCost * $request->tradedAmount);


        $trade = Trade::create([
            'user_id' => $user->id,
            'traded_coin' => $tradedCoin->symbol,
            'received_coin' => $receivedCoin->symbol,
            'traded_amount' => $request->tradedAmount,
            'received_amount' => $request->receivedAmount,
            'date' => $time_gmt,
            'btc_at_trade' => $btc_historical_price,
            'net_at_trade_usd' => $net_at_trade_usd,
            'btc_ratio' => $request->btcRatio
        ]);

        $traded = Transaction::create([
            'user_id' => $user->id,
            'coin_symbol' => $tradedCoin->symbol,
            'amount' => (-1 * $request->tradedAmount),
            'usd' => $tradedAvgCost,
            'trade_id' => $trade->id,
            'date' => $time_gmt
        ]);

        $received = Transaction::create([
            'user_id' => $user->id,
            'coin_symbol' => 'BTC',
            'amount' => $request->receivedAmount,
            'usd' => $btc_historical_price,
            'trade_id' => $trade->id,
            'date' => $time_gmt
        ]);

        FixAverageTradeCosts::dispatch($received);
        FixAverageSaleCosts::dispatch($received->user_id, $received->coin_symbol, $received->date)->delay(Carbon::now()->addSeconds(2));
        return response()->json('success');

    }

    public function ethToCoin(Request $request)
    {
        $this->validate($request, [
            'tradedCoin' => 'required',
            'tradedAmount' => 'required',
            'receivedCoin' => 'required',
            'receivedAmount' => 'required',
            'ethRatio' => 'required',
            'date' => 'required',
            'hour' => 'required',
            'minute' => 'required',
            'amPm' => 'required',
        ]);
        // Get User
        $user = Auth::user();
        // Parse and format datetime -> GMT
        $date = Carbon::parse($request->date);
        $date = $date->toDateString();

        if ($request->amPm == 'PM' && $request->hour != 12)
        {
            $hour = $request->hour + 12;
        } else {
            $hour = $request->hour;
        }

        $parse = Carbon::parse($date.' '.$hour.':'.$request->minute.':00');
        $datetime = Carbon::createFromFormat('Y-m-d H:i:s', $parse, $user->timezone);
        $datetime = $datetime->timezone('GMT');
        // Time for DB
        $time_gmt = $datetime->toDateTimeString();

        // Get traded and received coin.
        $tradedCoin = Coin::where('symbol', '=', $request->tradedCoin)->first();
        $receivedCoin = Coin::where('symbol', '=', $request->receivedCoin)->first();
        // Get avg cost value for traded coin from Holdings
        $tradedAvgCost = $this->getAvgTradeCost($tradedCoin->symbol, $user->id, $time_gmt);
        // Get cost value at time of trade from API
        $start = $datetime->format('Y-m-d\TH:i:s\Z');
        $end = $datetime->addMinutes(10)->format('Y-m-d\TH:i:s\Z');
        $granularity = (int)60;

        $url = 'https://api.gdax.com/products/ETH-USD/candles/?start='.$start.'&end='.$end.'&granularity='.$granularity;
        $response = Curl::to($url)
            ->withContentType('application/json')
            ->withHeader('User-Agent: Test')
            ->returnResponseObject()
            ->get();

        $ethereum_history = json_decode($response->content);
        $i = 0;
        $sum = 0;
        foreach ($ethereum_history as $unit)
        {
            // Average the low and high price and add to sum
            $average = ($unit[1] + $unit[2])/2;
            $sum += $average;
            $i++;
        }
        $eth_historical_price = $sum/$i;


        $received_usd = $eth_historical_price * $request->ethRatio;
        $net_at_trade_usd = ($eth_historical_price * $request->tradedAmount) - ($tradedAvgCost * $request->tradedAmount);


        $trade = Trade::create([
            'user_id' => $user->id,
            'traded_coin' => $tradedCoin->symbol,
            'received_coin' => $receivedCoin->symbol,
            'traded_amount' => $request->tradedAmount,
            'received_amount' => $request->receivedAmount,
            'date' => $time_gmt,
            'btc_at_trade' => $eth_historical_price,
            'net_at_trade_usd' => $net_at_trade_usd,
            'btc_ratio' => $request->ethRatio
        ]);

        $traded = Transaction::create([
            'user_id' => $user->id,
            'coin_symbol' => 'ETH',
            'amount' => (-1 * $request->tradedAmount),
            'usd' => $tradedAvgCost,
            'trade_id' => $trade->id,
            'date' => $time_gmt
        ]);

        $received = Transaction::create([
            'user_id' => $user->id,
            'coin_symbol' => $receivedCoin->symbol,
            'amount' => $request->receivedAmount,
            'usd' => $received_usd,
            'trade_id' => $trade->id,
            'date' => $time_gmt
        ]);


        FixAverageTradeCosts::dispatch($received);
        FixAverageSaleCosts::dispatch($received->user_id, $received->coin_symbol, $received->date)->delay(Carbon::now()->addSeconds(2));
        return response()->json('success');

    }

    public function coinToEth(Request $request)
    {
        $this->validate($request, [
            'tradedCoin' => 'required',
            'tradedAmount' => 'required',
            'receivedCoin' => 'required',
            'receivedAmount' => 'required',
            'ethRatio' => 'required',
            'date' => 'required',
            'hour' => 'required',
            'minute' => 'required',
            'amPm' => 'required',
        ]);
        // Get User
        $user = Auth::user();
        // Parse and format datetime -> GMT
        $date = Carbon::parse($request->date);
        $date = $date->toDateString();

        if ($request->amPm == 'PM' && $request->hour != 12)
        {
            $hour = $request->hour + 12;
        } else {
            $hour = $request->hour;
        }

        $parse = Carbon::parse($date.' '.$hour.':'.$request->minute.':00');
        $datetime = Carbon::createFromFormat('Y-m-d H:i:s', $parse, $user->timezone);
        $datetime = $datetime->timezone('GMT');
        $time_gmt = $datetime->toDateTimeString();

        // Get traded and received coin.
        $tradedCoin = Coin::where('symbol', '=', $request->tradedCoin)->first();
        $receivedCoin = Coin::where('symbol', '=', $request->receivedCoin)->first();
        // Get avg cost value for traded coin from Holdings
        $tradedAvgCost = $this->getAvgTradeCost($tradedCoin->symbol, $user->id, $time_gmt);
        // Get cost value at time of trade from API
        $start = $datetime->format('Y-m-d\TH:i:s\Z');
        $end = $datetime->addMinutes(10)->format('Y-m-d\TH:i:s\Z');
        $granularity = (int)60;

        $url = 'https://api.gdax.com/products/ETH-USD/candles/?start='.$start.'&end='.$end.'&granularity='.$granularity;
        $response = Curl::to($url)
            ->withContentType('application/json')
            ->withHeader('User-Agent: Test')
            ->returnResponseObject()
            ->get();

        $ethereum_history = json_decode($response->content);
        $i = 0;
        $sum = 0;
        foreach ($ethereum_history as $unit)
        {
            // Average the low and high price and add to sum
            $average = ($unit[1] + $unit[2])/2;
            $sum += $average;
            $i++;
        }
        $eth_historical_price = $sum/$i;

        $traded_usd = $eth_historical_price * $request->ethRatio;
        $net_at_trade_usd = ($traded_usd * $request->tradedAmount) - ($tradedAvgCost * $request->tradedAmount);


        $trade = Trade::create([
            'user_id' => $user->id,
            'traded_coin' => $tradedCoin->symbol,
            'received_coin' => $receivedCoin->symbol,
            'traded_amount' => $request->tradedAmount,
            'received_amount' => $request->receivedAmount,
            'date' => $time_gmt,
            'btc_at_trade' => $eth_historical_price,
            'net_at_trade_usd' => $net_at_trade_usd,
            'btc_ratio' => $request->ethRatio
        ]);

        $traded = Transaction::create([
            'user_id' => $user->id,
            'coin_symbol' => $tradedCoin->symbol,
            'amount' => (-1 * $request->tradedAmount),
            'usd' => $tradedAvgCost,
            'trade_id' => $trade->id,
            'date' => $time_gmt
        ]);

        $received = Transaction::create([
            'user_id' => $user->id,
            'coin_symbol' => 'ETH',
            'amount' => $request->receivedAmount,
            'usd' => $eth_historical_price,
            'trade_id' => $trade->id,
            'date' => $time_gmt
        ]);

        FixAverageTradeCosts::dispatch($received);
        FixAverageSaleCosts::dispatch($received->user_id, $received->coin_symbol, $received->date)->delay(Carbon::now()->addSeconds(2));

        return response()->json('success');

    }

    public function getTrades()
    {
        $user = Auth::user();
        $trades = Trade::with('transactions', 'tradedCoin', 'receivedCoin')
            ->where('user_id', '=', $user->id)
            ->orderBy('date', 'asc')
            ->get();
        foreach ($trades as $trade)
        {
            $trade->date = localDateTime($trade->date, $user->timezone);
        }
        $total_net_at_trade = $user->totalNetAtTrade();
//        $trades->put('total_net', $total_net_at_trade);
        return response()->json(array('trades'=>$trades,'total_net'=>$total_net_at_trade));
    }

    public function getAvgTradeCost($coin_symbol, $user_id, $date)
    {
        $beforeTransactions = Transaction::where('user_id', '=', $user_id)
            ->where('coin_symbol', '=', $coin_symbol)
            ->where('date', '<=', $date)
            ->where('amount', '>', 0)
            ->get();
        $sum = 0;
        $amount = 0;
        foreach ($beforeTransactions as $before)
        {
            $sum += ($before->amount * $before->usd);
            $amount += $before->amount;
        }

        if ($amount > 0)
        {
            $avg = $sum / $amount;
        } else {
            $avg = 0;
        }
        return (float)$avg;
    }



}
