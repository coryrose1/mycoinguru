<?php

namespace App\Http\Controllers;

use App\GdaxImport;
use App\Holdings;
use App\Jobs\FixAverageSaleCosts;
use App\Jobs\FixAverageTradeCosts;
use App\Sale;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'amount' => 'required',
            'coin' => 'required',
            'usd' => 'required',
            'fees' => 'required',
            'date' => 'required',
            'hour' => 'required',
            'minute' => 'required',
            'amPm' => 'required'
        ]);

        $user = Auth::user();
        $date = Carbon::parse($request->date);
        $date = $date->toDateString();

        if ($request->amPm == 'PM' && $request->hour != 12)
        {
            $hour = $request->hour + 12;
        } else {
            $hour = $request->hour;
        }

        $parse = Carbon::parse($date.' '.$hour.':'.$request->minute.':00');
        $datetime = Carbon::createFromFormat('Y-m-d H:i:s', $parse, $user->timezone);
        $datetime = $datetime->timezone('GMT');
        $time_gmt = $datetime->toDateTimeString();

        $sold_avg_cost = Holdings::select('usd')->where('coin_symbol', '=', $request->coin)
            ->where('user_id', '=', $user->id)
            ->first();
        $total = $request->usd * $request->amount;
        $profit = $total - ($sold_avg_cost->usd * $request->amount);

        $sale = Sale::create([
            'user_id' => $user->id,
            'coin_symbol' => $request->coin,
            'amount' => $request->amount,
            'usd' => $request->usd,
            'fees' => $request->fees,
            'total' => $total,
            'profit' => $profit,
            'avg_cost' => $sold_avg_cost->usd,
            'date' => $time_gmt
        ]);

        return 'success';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sale = Sale::find($id);
        FixAverageTradeCosts::dispatch($sale)->delay(Carbon::now()->addSeconds(2));
        FixAverageSaleCosts::dispatch($sale->user_id, $sale->coin_symbol, $sale->date)->delay(Carbon::now()->addSeconds(4));
        if ($sale->gdaxImport)
        {
            $import = GdaxImport::find($sale->gdaxImport->id);
            $import->imported = 0;
            $import->sale_id = null;
            $import->save();
        }
        if ($sale->coinbaseImport)
        {
            $import = GdaxImport::find($sale->coinbaseImport->id);
            $import->imported = 0;
            $import->sale_id = null;
            $import->save();
        }
        $sale->delete();
        return response()->json('deleted');
    }

    public function getSales()
    {
        $user = Auth::user();
        $sales = Sale::with('coin')
            ->where('user_id', '=', $user->id)
            ->get();
        foreach ($sales as $sale)
        {
            $sale->date = localDateTime($sale->date, $user->timezone);
        }
        $total_profit = $sales->sum('profit');

        return response()->json(array('sales'=>$sales,'total_profit'=>$total_profit));
    }
}
