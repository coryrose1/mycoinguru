<?php

namespace App\Http\Controllers;

use App\Coin;
use App\Holdings;
use App\Trade;
use App\User;
use App\Transaction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.dashboard');
    }

    public function isAdmin()
    {
        $user = Auth::user();
        return response()->json($user->isAdmin());
    }

    public function darkMode()
    {
        $user = Auth::user();
        if ($user->dark_mode == 0)
        {
            $user->dark_mode = 1;
            $user->save();
        } else
        {
            $user->dark_mode = 0;
            $user->save();
        }
        return redirect()->route('dashboard');
    }

    public function stats()
    {
        $user = User::find(Auth::user()->id);
        $stats = collect([
            'cost_basis' => $user->costBasis(),
            'revenue' => $user->revenue(),
            'profit' => $user->profit(),
            'roi' => $user->roi(),
            'original_investment' => $user->originalInvestment(),
            'original_profit' => $user->revenue() - $user->originalInvestment(),
            'original_roi' => round(((($user->revenue() - $user->originalInvestment()) / $user->originalInvestment()) * 100), 2)
        ]);
        return response()->json($stats);
    }

    public function byCoin()
    {
        $user = Auth::user();
        $holdings = Holdings::with('coin')
            ->where('user_id', '=', $user->id)
            ->where('amount', '>', 0)
            ->get();

        foreach ($holdings as $key => $holding)
        {
            $holdings[$key]['investment'] = $holding->investment();
            $holdings[$key]['profit'] = $holding->profit();
            $holdings[$key]['total_revenue'] = $holding->revenue();
            $holdings[$key]['roi'] = $holding->roi();
            $holdings[$key]['percentage'] = $user->holdingPercent($holding->coin_symbol);
        }
        $data = [
            'hide_low' => $user->hide_low_holdings,
            'holdings' => $holdings,
        ];
        return response()->json($data);
    }

    public function byPurchase()
    {
        $user = Auth::user();
        $holdings = Transaction::with('coin', 'trade')->where('user_id', '=', $user->id)->get();
        foreach ($holdings as $key => $holding)
        {
            // Convert date time to local timezone
            $holding->date = localDateTime($holding->date, $user->timezone);
            // Get invested (USD)
            $holding->invested = $holding->amount * $holding->usd;
            // Get action for filter
            if (empty($holding->trade))
            {
                $holding->action = 'Buy';
            } elseif ($holding->amount < 0)
            {
                $holding->action = 'Trade Sold';
            } else {
                $holding->action = 'Trade Received';
            }
        }
        return response()->json($holdings);
    }

    public function getAllTransactions()
    {
        $user = User::with('transactions', 'trades', 'sales')->find(Auth::user()->id);
        $transactions = collect();
        foreach ($user->transactions as $t)
        {
            if (is_null($t->trade_id))
            {
                $t->type = 'buy';
                $t->from = 'USD';
                $t->to = $t->coin_symbol;
                $t->from_amount = $t->usd * $t->amount;
                $t->to_amount = $t->amount;
                $t->from_price = 1;
                $t->to_price = $t->usd;
                $t->value = $t->usd * $t->amount;
                $transactions->push($t);
            }

        }
        foreach ($user->trades as $t)
        {
            $value = $t->transactions[1]->usd * $t->transactions[1]->amount;
            $t->type = 'trade';
            $t->from = $t->traded_coin;
            $t->to = $t->received_coin;
            $t->from_amount = $t->traded_amount;
            $t->to_amount = $t->received_amount;
            if ($t->traded_coin == 'ETH' || $t->traded_coin == 'BTC')
            {
                $t->from_price = (float)$t->btc_at_trade;
            } elseif ($t->received_coin == 'ETH' || $t->received_coin == 'BTC')
            {
                $t->from_price = (float)$t->btc_at_trade * $t->btc_ratio;
            }
            $t->to_price = $t->transactions[1]->usd;
            $t->value = $value;
            $transactions->push($t);
        }
        foreach ($user->sales as $s)
        {
            $s->type = 'sale';
            $s->from = 'USD';
            $s->to = $s->coin_symbol;
            $s->from_amount = $s->usd;
            $s->to_amount = $s->amount;
            $s->from_price = $s->usd;
            $s->to_price = 1;
            $s->value = $s->usd * $s->amount;
            $transactions->push($s);
        }
        return response()->json($transactions);
    }

    public function breakdown()
    {
        $user = User::find(Auth::user()->id);
        $holdings = Holdings::with('coin')->where('user_id', '=', $user->id)->get();
        $data = [
            'volume' => [
                'labels' => [],
                'amounts' => [],
                'colors' => []
            ],
            'revenue' => [
                'labels' => [],
                'amounts' => [],
                'colors' => []
            ],
            'roi' => [
                'labels' => [],
                'amounts' => [],
                'colors' => []
            ],
            'breakdown' => [
                'labels' => [],
                'amounts' => [],
                'colors' => []
            ],
            'cardData' => [
                'revenue' => [],
                'volume' => [],
                'roi' => [],
                'breakdown' => []
            ]
        ];
        foreach ($holdings as $key => $holding)
        {
            $revenue = [
                'label' => $holding->coin->symbol,
                'amount' => (float)($holding->amount * $holding->coin->usd)
            ];
            $volume = [
                'label' => $holding->coin->symbol,
                'amount' => (float)($holding->amount)
            ];
            $roi = [
                'label' => $holding->coin->symbol,
                'amount' => (float)$holding->roi()
            ];
            $breakdown = [
                'label' => $holding->coin->symbol,
                'amount' => (float)number_format($user->holdingPercent($holding->coin->symbol),2)
            ];
            //cardData
            array_push($data['cardData']['revenue'], $revenue);
            array_push($data['cardData']['volume'], $volume);
            array_push($data['cardData']['roi'], $roi);
            array_push($data['cardData']['breakdown'], $breakdown);
            //chart Data
            array_push($data['volume']['labels'], $holding->coin->name);
            array_push($data['volume']['amounts'], $holding->amount);
            array_push($data['volume']['colors'], "hsl(".rand(0,359).",50%,70%)");
            if ($holding->roi() > 0){
                array_push($data['roi']['labels'], $holding->coin->name);
                array_push($data['roi']['amounts'], $holding->roi());
                array_push($data['roi']['colors'], "hsl(".rand(0,359).",50%,70%)");
            }
            array_push($data['revenue']['labels'], $holding->coin->name);
            array_push($data['revenue']['amounts'], (float)($holding->amount * $holding->coin->usd));
            array_push($data['revenue']['colors'], "hsl(".rand(0,359).",50%,70%)");

            array_push($data['breakdown']['labels'], $holding->coin->name);
            array_push($data['breakdown']['amounts'], (float)number_format($user->holdingPercent($holding->coin->symbol),2));
            array_push($data['breakdown']['colors'], "hsl(".rand(0,359).",50%,70%)");

        }
        return collect($data);
    }

    // Specific user coin amounts by week
    // Used with history line chart in portfolio
    public function history()
    {
        $transactions = Transaction::where('user_id', '=', 1)->get();
        $transactions = $transactions->groupBy('coin_symbol')->sortBy('date');
        $data = [];
        foreach ($transactions as $symbol => $coin_transactions)
        {
            $amount = 0;
            $initial_week = Carbon::parse($coin_transactions->min('date'))->startOfWeek()->subWeek();
            $total_weeks = $initial_week->diffInWeeks(Carbon::now()->startOfWeek());
            $i = 0;
            while ($i < $total_weeks)
            {
                $week = $initial_week->addWeek()->toDateString();
                $data[$symbol][$week] = [];
                foreach ($coin_transactions as $transaction)
                {
                    $transaction->date = Carbon::parse($transaction->date)->startOfWeek()->toDateString();
                    if ($transaction->date == $week)
                    {
                        $amount = $amount + $transaction->amount;
                        $data[$symbol][$week] = $amount;
                    } else {
                        $data[$symbol][$week] = $amount;
                    }
                }
                $i++;
            }
        }
        return response()->json($data);
    }

    public function coinDetail($coin_symbol)
    {
        $user = User::with('transactions', 'trades', 'sales', 'holdings')->find(Auth::user()->id);
        $transactions = collect();
        foreach ($user->transactions as $t)
        {
            if (is_null($t->trade_id) && $t->coin_symbol == $coin_symbol)
            {
                $t->type = 'buy';
                $t->from = 'USD';
                $t->to = $t->coin_symbol;
                $t->from_amount = $t->usd * $t->amount;
                $t->to_amount = $t->amount;
                $t->from_price = 1;
                $t->to_price = $t->usd;
                $t->value = $t->usd * $t->amount;
                $transactions->push($t);
            }

        }
        foreach ($user->trades as $t)
        {
            if ($t->traded_coin == $coin_symbol || $t->received_coin == $coin_symbol)
            {
                $value = $t->transactions[1]->usd * $t->transactions[1]->amount;
                $t->type = 'trade';
                $t->from = $t->traded_coin;
                $t->to = $t->received_coin;
                $t->from_amount = $t->traded_amount;
                $t->to_amount = $t->received_amount;
                $t->to_price = $t->transactions[1]->usd;
                $t->value = $value;
                if ($t->traded_coin == 'ETH' || $t->traded_coin == 'BTC')
                {
                    $t->from_price = (float)$t->btc_at_trade;
                } elseif ($t->received_coin == 'ETH' || $t->received_coin == 'BTC')
                {
                    $t->from_price = (float)$t->btc_at_trade * $t->btc_ratio;
                }
                $transactions->push($t);
            }
        }
        foreach ($user->sales as $s)
        {
            if ($s->coin_symbol == $coin_symbol)
            {
                $s->type = 'sale';
                $s->from = $s->coin_symbol;
                $s->to = 'USD';
                $s->from_amount = $s->amount;
                $s->to_amount = $s->usd * $s->amount;
                $s->from_price = $s->usd;
                $s->to_price = 1;
                $s->value = $s->usd * $s->amount;
                $transactions->push($s);
            }
        }
        $average = $user->holdings->where('coin_symbol', '=', $coin_symbol)->first()->usd;
        $investment = $user->holdings->where('coin_symbol', '=', $coin_symbol)->first()->investment();
        $profit = $user->profitOnCoin($coin_symbol);
        $data = [
            'roi' => (float)$user->roiOnCoin($coin_symbol),
            'average' => (float)$average,
            'investment' => (float)$investment,
            'profit' => (float)$profit,
            'transactions' => $transactions
        ];
        $data = collect($data);
        return response()->json($data);
    }
    
    public function hideLowHoldings(Request $request)
    {
        $user = Auth::user();
        if ($request->hide == true)
        {
            $user->hide_low_holdings = 1;
            $user->save();
        } else {
            $user->hide_low_holdings = 0;
            $user->save();
        }
        return response()->json('success');
    }

}
