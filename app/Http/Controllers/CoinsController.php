<?php

namespace App\Http\Controllers;

use App\Coin;
use Illuminate\Http\Request;
use Ixudra\Curl\Facades\Curl;

class CoinsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('coins');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function get()
    {
        $coins = Coin::whereNotNull('rank')->orderBy('rank','asc')->get();
        $coinsArray = [];
        foreach ($coins as $coin)
        {
            $coinsArray[] = [
                'rank' => $coin->rank,
                'symbol' => $coin->symbol,
                'icon' => $coin->icon,
                'name' => $coin->name,
                'usd' => (float)$coin->usd,
                'btc' => (float)$coin->btc,
                'volume_24h_usd' => $coin->volume_24h_usd,
                'market_cap' => (float)$coin->market_cap_usd,
                'percent_change_1h' => (float)$coin->percent_change_1h,
                'percent_change_24h' => (float)$coin->percent_change_24h,
                'percent_change_7d' => (float)$coin->percent_change_7d,
                'cmc_id' => $coin->cmc_id,
            ];
        }
        return response()->json($coinsArray);
    }

    public function getSymbols()
    {
        $coins = Coin::select('symbol')->whereNotNull('rank')->orderBy('rank','asc')->get();
        return response()->json($coins);
    }
}
