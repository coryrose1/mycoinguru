<?php

namespace App\Http\Controllers;

use App\GdaxImport;
use App\Jobs\FixAverageSaleCosts;
use App\Jobs\FixAverageTradeCosts;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Transaction;
use Illuminate\Support\Facades\Auth;

class TransactionsController extends Controller
{

    public function store(Request $request)
    {
        $this->validate($request, [
            'amount' => 'required',
            'coin' => 'required',
            'usd' => 'required',
            'date' => 'required',
            'hour' => 'required',
            'minute' => 'required',
            'amPm' => 'required'
        ]);

        $user = Auth::user();
        $date = Carbon::parse($request->date);
        $date = $date->toDateString();

        if ($request->amPm == 'PM' && $request->hour != 12)
        {
            $hour = $request->hour + 12;
        } else {
            $hour = $request->hour;
        }

        $parse = Carbon::parse($date.' '.$hour.':'.$request->minute.':00');
        $datetime = Carbon::createFromFormat('Y-m-d H:i:s', $parse, $user->timezone);
        $datetime = $datetime->timezone('GMT');
        $time_gmt = $datetime->toDateTimeString();

        $transaction = Transaction::create([
            'user_id' => $user->id,
            'coin_symbol' => $request->coin,
            'amount' => $request->amount,
            'usd' => ($request->usd / $request->amount),
            'fees_usd' => $request->fees,
            'date' => $time_gmt
        ]);

        FixAverageTradeCosts::dispatch($transaction);
        FixAverageSaleCosts::dispatch($user->id, $request->coin, $time_gmt)->delay(Carbon::now()->addSeconds(2));

        return response()->json('success');
    }

    public function destroy($id)
    {
        $purchase = Transaction::find($id);
        FixAverageTradeCosts::dispatch($purchase)->delay(Carbon::now()->addSeconds(2));
        FixAverageSaleCosts::dispatch($purchase->user_id, $purchase->coin_symbol, $purchase->date)->delay(Carbon::now()->addSeconds(4));
        if ($purchase->gdaxImport)
        {
            $import = GdaxImport::find($purchase->gdaxImport->id);
            $import->imported = 0;
            $import->transaction_id = null;
            $import->save();
        }
        if ($purchase->coinbaseImport)
        {
            $import = GdaxImport::find($purchase->coinbaseImport->id);
            $import->imported = 0;
            $import->transaction_id = null;
            $import->save();
        }
        $purchase->delete();
        return response()->json('deleted');
    }
}
