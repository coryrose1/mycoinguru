<?php

namespace App\Http\Controllers;

use App\Watchlist;
use App\Coin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WatchlistController extends Controller
{
    public function getWatchlist()
    {
        if (Auth::check())
        {
        $user = Auth::user();
        return response()->json($user->watchlist);
        }
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'id' => 'required'
        ]);

        $user = Auth::user();

        $watchlist = Watchlist::create([
            'user_id' => $user->id,
            'coin_id' => $request->id
        ]);

        return response()->json('success');
    }

    public function destroy($coin_id)
    {
        $user = Auth::user();
        $watchlist = Watchlist::where('user_id', '=', $user->id)
            ->where('coin_id', '=', $coin_id)
            ->delete();
        return response()->json('success');
    }

    public function availableCoins()
    {
        $watchlist = Watchlist::where('user_id', '=', Auth::user()->id)->get();
        $watchlist_coins = $watchlist->pluck('coin_id');
        $coins = Coin::whereNotIn('id', $watchlist_coins)->get();
        return response()->json($coins);
    }
}
