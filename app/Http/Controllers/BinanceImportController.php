<?php

namespace App\Http\Controllers;

use App\BinanceImport;
use App\Jobs\RunBinanceImport;
use App\Trade;
use App\Transaction;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Excel;
use Illuminate\Support\Facades\Auth;
use Larislackers\BinanceApi\BinanceApiContainer;


class BinanceImportController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getFromApi()
    {
        $user = Auth::user();
        if (count($user->binanceApi) > 0)
        {

        } else
        {
            return redirect('/settings/api');
        }
    }

    public function importFromExcel(Request $request)
    {
        Excel::load($request->file('binanceFile')->getRealPath(), function($reader) {

            // Getting all results
            $results = $reader->get();
//            foreach ($results as $row){
//                if ($row['status'] == "Filled"){
//                    dump($row);
//                }
//            }
//            dd($results);
            $user = User::find(Auth::user()->id);
//            $existingImports = BinanceImport::select('user_id','date')->where('user_id', '=', $user->id)->get();
//            dd($results);
            foreach ($results as $key => $row)
            {

                    // Retrieve filled orders
                    if ($row['status'] == "Filled" || $row['status'] == "Partial Fill")
                    {
                        // ETH trades or BTC trades
                        if (strpos($row['pair'], 'ETH')) {
                            $coins = explode("ETH", $row['pair']);
                            $coin_1 = "ETH";
                            $coin_2 = $coins[0];
                            // Binance vs CMC Symbol difference check
                            if ($coin_2 == "IOTA")
                            {
                                $coin_2 = "MIOTA";
                            }
                            if ($coin_2 == "ST")
                            {
                                $coin_2 = "OST";
                            }
                        } elseif (strpos($row['pair'], 'BTC'))
                        {
                            $coins = explode("BTC", $row['pair']);
                            $coin_1 = "BTC";
                            $coin_2 = $coins[0];
                            // Binance vs CMC Symbol difference check
                            if ($coin_2 == "IOTA")
                            {
                                $coin_2 = "MIOTA";
                            }
                            if ($coin_2 == "ST")
                            {
                                $coin_2 = "OST";
                            }
                        }
                        $binanceImport = BinanceImport::updateOrCreate(
                            [
                                'user_id' => $user->id,
                                'coin_1' => $coin_1,
                                'coin_2' => $coin_2,
                                'date' => Carbon::parse($row['dateutc'])->toDateTimeString()
                            ],
                            [
                            'action' => $row['type'],
                            'amount' => (float)$row['filled'],
                            'total' => (float)$row['total'],
                            'price' => (float)$row['total'] / (float)$row['filled'],
                        ]);
                    }
                }
        });

        return redirect()->route('binance');
    }

    public function retrieveImport()
    {
        $binanceImport = BinanceImport::where('user_id', '=', Auth::user()->id)
            ->where('imported', '=', 0)
            ->orderBy('date', 'asc')
            ->get();
        if ($binanceImport)
        {
            return response()->json($binanceImport);
        }
    }

    public function handleImport(Request $request)
    {
        $user = Auth::user();
        $seconds = 5;
        $trades = collect($request->trades)->sortBy('date');
//        dd($trades);
        foreach ($trades as $import)
        {
            $import = BinanceImport::find($import['id']);
            RunBinanceImport::dispatch($user, $import)->delay(Carbon::now()->addSeconds($seconds));
            $seconds = $seconds + 2;
        }
        return response()->json('success');
    }

    public function retrieveMissingTrades()
    {
        $imports = BinanceImport::where('imported', 1)->whereNull('trade_id')->get();
        foreach ($imports as $import)
        {
            if ($import->action == 'BUY')
            {
                $trade = Trade::where('user_id', $import->user_id)
                    ->where('traded_coin', $import->coin_1)
                    ->where('received_coin', $import->coin_2)
                    ->where('traded_amount', $import->total)
                    ->where('date', $import->date)
                    ->first();
                if ($trade)
                {
                    $import->trade_id = $trade->id;
                    $import->save();
                }

            } else
            {
                $trade = Trade::where('user_id', $import->user_id)
                    ->where('traded_coin', $import->coin_2)
                    ->where('received_coin', $import->coin_1)
                    ->where('traded_amount', $import->amount)
                    ->where('date', $import->date)
                    ->first();
                if ($trade)
                {
                    $import->trade_id = $trade->id;
                    $import->save();
                }
            }
        }
    }

}
