<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SettingsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        return view('settings.general');
    }
    public function api()
    {
        $user = User::with('gdaxApi', 'coinbaseApi', 'binanceApi')->find(Auth::id());
//        dd($user);
        return view('settings.api', compact('user'));
    }
}
