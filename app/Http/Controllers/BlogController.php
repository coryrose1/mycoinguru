<?php

namespace App\Http\Controllers;




use App\Post;
use App\PostArchives;
use App\Tag;
use Illuminate\Support\Facades\DB;

class BlogController extends Controller
{
    public function index()
    {
        $posts = Post::with('author')->orderBy('published_at','desc')->get();
        $archives = PostArchives::get();
        $tags = Tag::all();
        return view('blog.blog', compact('posts', 'tags', 'archives'));
    }

    public function posts()
    {
        return Post::paginate(5);
    }

    public function show($id)
    {
        return Post::find($id);
    }

}
