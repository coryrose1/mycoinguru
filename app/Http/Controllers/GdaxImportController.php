<?php

namespace App\Http\Controllers;

use App\GdaxApi;
use App\GdaxImport;
use App\Jobs\FixAverageSaleCosts;
use App\Jobs\FixAverageTradeCosts;
use App\Jobs\RunGdaxImport;
use App\Sale;
use App\Trade;
use App\Transaction;
use Carbon\Carbon;
use GDAX\Clients\PublicClient;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class GdaxImportController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function userApiExists($user)
    {
        if ($user->gdaxApi)
        {
            return true;
        } else
        {
            return false;
        }
    }

    public function getFromApi()
    {
        $user = Auth::user();
        if ($this->userApiExists($user))
        {
            // First retrieve GDAX products
            $products = ['BTC-USD','BCH-USD','ETH-USD','ETH-BTC','LTC-USD','LTC-BTC'];
            // Now create authenticated client
            $client = new \GDAX\Clients\AuthenticatedClient(
                $user->gdaxApi->api_key,
                $user->gdaxApi->api_key_secret,
                $user->gdaxApi->api_key_passphrase
            );

            foreach ($products as $product)
            {
                $coins = explode('-', $product);
                $fill = (new \GDAX\Types\Request\Authenticated\Fill())
                    ->setProductId($product);

                $fillData = $client->getFills($fill);
                if ($fillData !== null)
                {
                    foreach ($fillData as $order)
                    {
                        $refObj = new \ReflectionObject($order);
                        // price, size, date, side, settled, fee
                        $date = Carbon::instance($order->getCreatedAt())->toDateTimeString();
                        $refTrade = $refObj->getProperty('trade_id');
                        $refTrade->setAccessible(TRUE);
                        $trade_id = $refTrade->getValue($order);
                        $refOrder = $refObj->getProperty('order_id');
                        $refOrder->setAccessible(TRUE);
                        $order_id = $refOrder->getValue($order);
                        $refPrice = $refObj->getProperty('price');
                        $refPrice->setAccessible(TRUE);
                        $price = $refPrice->getValue($order);
                        $refSize = $refObj->getProperty('size');
                        $refSize->setAccessible(TRUE);
                        $size = $refSize->getValue($order);
                        $refSide = $refObj->getProperty('side');
                        $refSide->setAccessible(TRUE);
                        $side = $refSide->getValue($order);
                        $refFee = $refObj->getProperty('fee');
                        $refFee->setAccessible(TRUE);
                        $fee = $refFee->getValue($order);
                        $refSettled = $refObj->getProperty('settled');
                        $refSettled->setAccessible(TRUE);
                        $settled = $refSettled->getValue($order);
                        $refUsdVolume = $refObj->getProperty('usd_volume');
                        $refUsdVolume->setAccessible(TRUE);
                        $usdVolume = $refUsdVolume->getValue($order);

                        if ($settled == true)
                        {
                            // Buy or Sell
                            if ($coins[1] == 'USD')
                            {
                                $row = GdaxImport::updateOrCreate(
                                    [
                                        'gdax_trade_id' => $trade_id,
                                        'gdax_order_id' => $order_id
                                    ],
                                    [
                                    'user_id' => $user->id,
                                    'coin_1' => $coins[0],
                                    'usd' => $price,
                                    'date' => $date,
                                    'action' => $side,
                                    'amount' => $size,
                                    'fees_usd' => $fee,
                                    'coin_2' => 'USD'
                                ]);
                            } else {
                                if ($side == 'buy')
                                {
                                    $row = GdaxImport::updateOrCreate(
                                        [
                                            'gdax_trade_id' => $trade_id,
                                            'gdax_order_id' => $order_id
                                        ],
                                        [
                                        'user_id' => $user->id,
                                        'coin_1' => $coins[1],
                                        'coin_2' => $coins[0],
                                        'date' => $date,
                                        'traded_amount' => $price * $size,
                                        'action' => 'trade',
                                        'received_amount' => $size,
                                        'fees_usd' => $fee,
                                    ]);
                                } else {
                                    $row = GdaxImport::updateOrCreate(
                                        [
                                            'gdax_trade_id' => $trade_id,
                                            'gdax_order_id' => $order_id
                                        ],
                                        [
                                        'user_id' => $user->id,
                                        'coin_1' => $coins[0],
                                        'coin_2' => $coins[1],
                                        'date' => $date,
                                        'received_amount' => $size * $price,
                                        'action' => 'trade',
                                        'traded_amount' => $size,
                                        'fees_usd' => $fee,
                                    ]);
                                }
                            }
                        }
                    }
                }
            }
            return view('dashboard.import-coins.gdax-import');
        } else
        {
            return redirect('/settings/api');
        }

    }

    // Retrieve import rows from table
    public function table()
    {
        $gdaxImport = GdaxImport::where('user_id', '=', Auth::user()->id)
            ->where('imported', '=', 0)
            ->orderBy('date', 'asc')
            ->get();
        return response()->json($gdaxImport);
    }

    public function handleImport(Request $request)
    {
        $user = Auth::user();
        $seconds = 5;
        foreach ($request->import as $import)
        {
            RunGdaxImport::dispatch($user, $import['id'])->delay(Carbon::now()->addSeconds($seconds));
            $seconds = $seconds + 3;
        }
        return response()->json('success');
    }

    public function markAsImported(Request $request)
    {
        $user = Auth::user();
        foreach ($request->import as $import)
        {
            $existingRow = GdaxImport::where('gdax_trade_id', $import['gdax_trade_id'])
                ->where('gdax_order_id', $import['gdax_order_id'])
                ->where('user_id', $user->id)
                ->first();
            $existingRow->imported = 1;
            $existingRow->save();
        }
        return response()->json('success');
    }

    public function retrieveImported()
    {
        $gdaxImport = GdaxImport::where('user_id', '=', Auth::id())
            ->where('imported', '=', 1)
            ->orderBy('date', 'asc')
            ->get();
        return response()->json($gdaxImport);
    }

    public function deletePrevious(Request $request)
    {
        $user = Auth::user();
        foreach ($request->import as $import)
        {
            $existingImportRow = GdaxImport::where('gdax_trade_id', $import['gdax_trade_id'])
                ->where('gdax_order_id', $import['gdax_order_id'])
                ->where('user_id', $user->id)
                ->first();
            $existingImportRow->imported = 0;
            if ($existingImportRow->action == 'buy')
            {
                if ($existingImportRow->transaction_id)
                {
                    $buy = Transaction::find($existingImportRow->transaction_id);
                    FixAverageTradeCosts::dispatch($buy);
                    FixAverageSaleCosts::dispatch($buy->user_id, $buy->coin_symbol, $buy->date);
                    $existingImportRow->transaction_id = null;
                    $buy->delete();
                }
            } elseif ($existingImportRow->action == 'sell')
            {
                if ($existingImportRow->sale_id)
                {
                    $sell = Sale::find($existingImportRow->action->sale_id);
                    FixAverageTradeCosts::dispatch($sell);
                    FixAverageSaleCosts::dispatch($sell->user_id, $sell->coin_symbol, $sell->date);
                    $existingImportRow->sale_id = null;
                    $sell->delete();
                }
            } else
            {
                if ($existingImportRow->trade_id)
                {
                    $trade = Trade::find($existingImportRow->trade_id);
                    FixAverageTradeCosts::dispatch($trade->transactions[1]);
                    FixAverageSaleCosts::dispatch($trade->transactions[1]->user_id, $trade->transactions[1]->coin_symbol, $trade->transactions[1]->date);
                    $existingImportRow->trade_id = null;
                    $trade->delete();
                }
            }
            $existingImportRow->save();
        }
        return response()->json('success');
    }

    public function retrieveMissingTransactions()
    {
        $users = \DB::table('gdax_imports')->select('user_id')->distinct()->get();
        foreach ($users as $user)
        {
            $imports = GdaxImport::where('user_id', $user->user_id)
                ->where('imported', 1)
                ->get();
            foreach ($imports as $import)
            {
                if ($import->action == 'trade')
                {
                    $trade = Trade::where('user_id', $user->user_id)
                        ->where('traded_coin', $import->coin_1)
                        ->where('received_coin', $import->coin_2)
                        ->where('traded_amount', $import->traded_amount)
                        ->where('received_amount', $import->received_amount)
                        ->where('date', $import->date)
                        ->first();

                    if ($trade)
                    {
                        $import->trade_id = $trade->id;
                        $import->save();
                    }
                } elseif ($import->action == 'buy')
                {
                    $transaction = Transaction::where('user_id', $user->user_id)
                        ->where('coin_symbol', $import->coin_1)
                        ->where('amount', $import->amount)
                        ->where('usd', $import->usd)
                        ->where('fees_usd', $import->fees_usd)
                        ->where('date', $import->date)
                        ->first();

                    if ($transaction)
                    {
                        $import->transaction_id = $transaction->id;
                        $import->save();
                    }
                } else
                {
                    $sale = Sale::where('user_id', $user->user_id)
                        ->where('coin_symbol', $import->coin_1)
                        ->where('amount', $import->amount)
                        ->where('usd', $import->usd)
                        ->where('fees', $import->fees_usd)
                        ->where('date', $import->date)
                        ->first();

                    if ($sale)
                    {
                        $import->sale_id = $sale->id;
                        $import->save();
                    }
                }
            }
        }
    }
}
