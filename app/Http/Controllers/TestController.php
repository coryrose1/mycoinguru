<?php

namespace App\Http\Controllers;

use App\BinanceApi;
use App\BinanceImport;
use App\Coin;
use Illuminate\Support\Facades\Hash;
use Ixudra\Curl\Facades\Curl;

class TestController extends Controller
{
    public function index()
    {
        dd(Hash::make('joeypaysthetrolltoll'));
    }

    public function userApiExists($user)
    {
        if (count($user->gdaxApi) > 0)
        {
            return true;
        } else
        {
            return false;
        }
    }

    public function socketTest()
    {
        $coins = Coin::take(1)->get();
        $socketData = '';
        $count = count($coins);
        $i = 1;
        foreach ($coins as $coin)
        {
            if ($count == $i)
            {
                $socketData .= "['5~CCCAGG~".$coin->symbol."~USD']";
            } else
            {
                if ($coin->symbol == 'BTC' || $coin->symbol == 'ETH')
                {
                    $socketData .= "['5~CCCAGG~".$coin->symbol."~USD'],";
                } else
                {
                    $socketData .= "['5~CCCAGG~".$coin->symbol."~BTC'],";
                }

            }

            $i++;
        }
        return response()->json($socketData);
    }
}
