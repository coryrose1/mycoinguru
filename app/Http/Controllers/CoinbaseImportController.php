<?php

namespace App\Http\Controllers;

use App\CoinbaseApi;
use App\CoinbaseImport;
use App\Jobs\FillCoinbaseImport;
use App\Jobs\FixAverageSaleCosts;
use App\Jobs\FixAverageTradeCosts;
use App\Jobs\RunCoinbaseImport;
use App\Sale;
use App\Transaction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Auth;
use Ixudra\Curl\Facades\Curl;
use Socialite;
use Coinbase\Wallet\Client;
use Coinbase\Wallet\Configuration;

class CoinbaseImportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function refreshToken()
    {
        $user = Auth::user();
        if ($user->coinbaseApi)
        {
            $api = CoinbaseApi::where('user_id', $user->id)->first();
                $response = Curl::to('https://api.coinbase.com/oauth/token')
                    ->withData([
                        'grant_type' => 'refresh_token',
                        'client_id' => env('COINBASE_KEY'),
                        'client_secret' => env('COINBASE_SECRET'),
                        'refresh_token' => $api->refresh_token
                    ])
                    ->returnResponseObject()
                    ->post();
                $content = json_decode($response->content);
                if ($api->access_token)
                {
                    $api->access_token = $content->access_token;
                    $api->refresh_token = $content->refresh_token;
                    $api->expires_at = Carbon::now()->addSeconds($content->expires_in);
                    $api->save();
                }
                return redirect()->route('coinbase.requery');

        } else
        {
            return redirect()->route('coinbase.oauth');
        }
    }

    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToCoinbase()
    {
        return Socialite::driver('coinbase')
            ->scopes(['wallet:transactions:read','wallet:accounts:read','wallet:buys:read','wallet:sells:read'])
            ->with(['account' => 'all'])
            ->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleCoinbaseCallback()
    {
        $oauth = Socialite::driver('coinbase')->user();
        $user = Auth::user();
        // with a refresh token
        CoinbaseApi::updateOrCreate(
            ['user_id' => $user->id],
            [
                'access_token' => $oauth->token,
                'refresh_token' => $oauth->refreshToken,
                'expires_at' => Carbon::now()->addSeconds($oauth->expiresIn)
            ]);

        return redirect()->route('settings.api');

    }

    public function retrieveImport()
    {
        $user = Auth::user();
        $imports = CoinbaseImport::select(['id', 'user_id', 'coin_symbol', 'amount', 'usd', 'fees_usd', 'date', 'action'])
            ->where('user_id', '=', $user->id)
            ->where('imported', '=', 0)
            ->orderBy('date', 'asc')
            ->get();
        foreach ($imports as $key => $import)
        {
            $imports[$key]->total = $import->amount * $import->usd;
        }
        return response()->json($imports);
    }

    public function handleImport(Request $request)
    {
        foreach($request->transactions as $transaction)
        {
            $import = CoinbaseImport::find($transaction['id']);
            if ($transaction['action'] == 'buy')
            {
                $buy = Transaction::create([
                    'user_id' => $transaction['user_id'],
                    'coin_symbol' => $transaction['coin_symbol'],
                    'amount' => $transaction['amount'],
                    'usd' => $transaction['usd'],
                    'fees_usd' => $transaction['fees_usd'],
                    'date' => $transaction['date']
                ]);
                FixAverageTradeCosts::dispatch($buy);
                $import->imported = 1;
                $import->transaction_id = $buy->id;
                $import->save();
            } else
            {
                $sell = Sale::create([
                    'user_id' => $transaction['user_id'],
                    'coin_symbol' => $transaction['coin_symbol'],
                    'amount' => $transaction['amount'],
                    'usd' => $transaction['usd'],
                    'fees_usd' => $transaction['fees_usd'],
                    'date' => $transaction['date']
                ]);
                $import->imported = 1;
                $import->sale_id = $sell->id;
                $import->save();
            }
        }
        return response()->json($request->transactions);
    }

    public function retrieveImported()
    {
        $coinbaseImport = CoinbaseImport::where('user_id', '=', Auth::id())
            ->where('imported', '=', 1)
            ->orderBy('date', 'asc')
            ->get();
        return response()->json($coinbaseImport);
    }

    public function deletePrevious(Request $request)
    {
        $user = Auth::user();
        foreach ($request->import as $import)
        {
            $existingImportRow = CoinbaseImport::where('coinbase_id', $import['coinbase_id'])
                ->where('user_id', $user->id)
                ->first();
            $existingImportRow->imported = 0;
            if ($existingImportRow->transaction_id)
            {
                $buy = Transaction::find($existingImportRow->transaction_id);
                FixAverageTradeCosts::dispatch($buy);
                FixAverageSaleCosts::dispatch($buy->user_id, $buy->coin_symbol, $buy->date);
                $existingImportRow->transaction_id = null;
                $buy->delete();
            }
            if ($existingImportRow->sale_id)
            {
                $sale = Sale::find($existingImportRow->sale_id);
                FixAverageTradeCosts::dispatch($sale);
                FixAverageSaleCosts::dispatch($sale->user_id, $sale->coin_symbol, $sale->date);
                $existingImportRow->sale_id = null;
                $sale->delete();
            }
            $existingImportRow->save();
        }
        return response()->json('success');
    }

    public function retrieveMissingTransactions()
    {
        $imports = CoinbaseImport::where('action', 'buy')
            ->where('imported', 1)
            ->whereNull('transaction_id')
            ->get();

        foreach ($imports as $import)
        {
            $transaction = Transaction::where('user_id', $import->user_id)
                ->where('coin_symbol', $import->coin_symbol)
                ->where('date', $import->date)
                ->where('amount', $import->amount)
                ->where('usd', $import->usd)
                ->first();
            if ($transaction)
            {
                $import->transaction_id = $transaction->id;
                $import->save();
            }
        }

        $saleImports = CoinbaseImport::where('action', 'sell')
            ->where('imported', 1)
            ->whereNull('sale_id')
            ->get();

        foreach ($saleImports as $import)
        {
            $sale = Sale::where('user_id', $import->user_id)
                ->where('coin_symbol', $import->coin_symbol)
                ->where('date', $import->date)
                ->where('amount', $import->amount)
                ->where('usd', $import->usd)
                ->first();
            if ($sale)
            {
                $import->sale_id = $sale->id;
                $import->save();
            }
        }
    }

    public function queryCoinbase()
    {
        $user = Auth::user();
        $oauth = CoinbaseApi::where('user_id', $user->id)->first();
        $configuration = Configuration::oauth($oauth->access_token, $oauth->refresh_token);


        $client = Client::create($configuration);
        $accounts = $client->getAccounts();
        foreach ($accounts as $acct)
        {
            $buys = $client->getAccountBuys($acct);
            foreach ($buys as $buy)
            {
                $raw = $buy->getRawData();
                $usd = round($raw['subtotal']['amount'] / $raw['amount']['amount'],8);
                $total_fees = 0;
                foreach ($raw['fees'] as $fee)
                {
                    $total_fees += $fee['amount']['amount'];
                }
                if ($raw['status'] == 'completed')
                {
                    $date = Carbon::parse($raw['created_at'])->toDateTimeString();
                    $existingImport = CoinbaseImport::where('user_id', $user->id)
                        ->where('coin_symbol', $raw['amount']['currency'])
                        ->where('usd', $usd)
                        ->where('action', 'buy')
                        ->where('amount', round($raw['amount']['amount'],8))
                        ->where('date', $date)
                        ->where('fees_usd', round($total_fees,8))
                        ->first();
                    if ($existingImport)
                    {
                        if ($existingImport->coinbase_id == NULL || $existingImport->coinbase_id == '')
                        {
                            $existingImport->coinbase_id = $raw['id'];
                            $existingImport->save();
                        }
                    } else {
//                        dump($raw);
                        $import = CoinbaseImport::updateOrCreate(
                            [
                                'coinbase_id' => $raw['id']
                            ],
                            [
                                'user_id' => $user->id,
                                'coin_symbol' => $raw['amount']['currency'],
                                'usd' => $usd,
                                'date' => $date,
                                'action' => 'buy',
                                'amount' => $raw['amount']['amount'],
                                'fees_usd' => $total_fees,
                            ]);
                    }
                }

            }
            $sells = $client->getSells($acct);
            foreach ($sells as $sell)
            {
                $raw = $sell->getRawData();
                $usd = round($raw['subtotal']['amount'] / $raw['amount']['amount'],8);
                $total_fees = 0;
                foreach ($raw['fees'] as $fee)
                {
                    $total_fees += $fee['amount']['amount'];
                }
                if ($raw['status'] == 'completed')
                {
                    $date = Carbon::parse($raw['created_at'])->toDateTimeString();
                    $existingImport = CoinbaseImport::where('user_id', $user->id)
                        ->where('coin_symbol', $raw['amount']['currency'])
                        ->where('usd', $usd)
                        ->where('action', 'sell')
                        ->where('amount', round($raw['amount']['amount'],8))
                        ->where('date', $date)
                        ->where('fees_usd', round($total_fees,8))
                        ->first();
                    if ($existingImport)
                    {
                        if ($existingImport->coinbase_id == NULL || $existingImport->coinbase_id == '')
                        {
                            $existingImport->coinbase_id = $raw['id'];
                            $existingImport->save();
                        }
                    } else
                    {
                        $import = CoinbaseImport::updateOrCreate(
                            [
                                'coinbase_id' => $raw['id']
                            ],
                            [
                                'user_id' => $user->id,
                                'coin_symbol' => $raw['amount']['currency'],
                                'usd' => $usd,
                                'date' => $date,
                                'action' => 'sell',
                                'amount' => $raw['amount']['amount'],
                                'fees_usd' => $total_fees,
                            ]);
                    }
                }
            }
        }
        return view('dashboard.import-coins.coinbase-import');
    }
}
