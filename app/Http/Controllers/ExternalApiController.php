<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Ixudra\Curl\Facades\Curl;

class ExternalApiController extends Controller
{

    public function cryptoCompare($coin, $priceIn)
    {
        $graphData = [];
        // Minute
        $url = 'https://min-api.cryptocompare.com/data/histominute?fsym='.$coin.'&tsym='.$priceIn.'&limit=2000&aggregate=3&e=CCCAGG';
        $data = Curl::to($url)
            ->returnResponseObject()
            ->get();
        $minuteContent = json_decode($data->content);
        array_push($graphData, $minuteContent->Data);

        // Hour
        $url = 'https://min-api.cryptocompare.com/data/histohour?fsym='.$coin.'&tsym='.$priceIn.'&limit=2000&aggregate=3&e=CCCAGG';
        $data = Curl::to($url)
            ->returnResponseObject()
            ->get();
        $hourlyContent = json_decode($data->content);
        array_push($graphData, $hourlyContent->Data);
        // Daily
        $url = 'https://min-api.cryptocompare.com/data/histoday?fsym='.$coin.'&tsym='.$priceIn.'&limit=2000&aggregate=3&e=CCCAGG';
        $data = Curl::to($url)
            ->returnResponseObject()
            ->get();
        $dailyContent = json_decode($data->content);
        array_push($graphData, $dailyContent->Data);
        $graphData = array_merge($graphData[0], $graphData[1], $graphData[2]);
        $i = 0;
        foreach ($graphData as $data)
        {
            $graphData[$i] = get_object_vars($data);
            $i++;
        }
        usort($graphData, "array_date_sort");

        $ohlc = [];
        $volume = [];
        $length = count($graphData);
        $groupingUnits = [
            ['minute' => [1,5,15]],
            ['hour' => [1,2,4]],
            ['day' => 1],
            ['week' => [1]],
        ];
        $i = 0;
        foreach ($graphData as $day)
        {
            $ohlc[$i][0] = ($day['time']*1000);
            $ohlc[$i][1] = $day['open'];
            $ohlc[$i][2] = $day['high'];
            $ohlc[$i][3] = $day['low'];
            $ohlc[$i][4] = $day['close'];
            $volume[$i][0] = ($day['time']*1000);
            $volume[$i][1] = $day['volumeto'];
            $i++;
        }
        $response = json_encode(collect([
            'ohlc' => $ohlc,
            'volume' => $volume,
            'length' => $length,
            'groupingUnits' => $groupingUnits
        ]),JSON_PRETTY_PRINT);
        return $response;
    }

    public function cryptoCompareHourly($coin, $priceIn)
    {
        $url = 'https://min-api.cryptocompare.com/data/histohour?fsym='.$coin.'&tsym='.$priceIn.'&limit=2000&aggregate=3&e=CCCAGG';
        $data = Curl::to($url)
            ->returnResponseObject()
            ->get();
        $content = json_decode($data->content);
        $ohlc = [];
        $volume = [];
        $length = count($content->Data);
        $groupingUnits = [
            ['minute' => [1,5,15]],
            ['hour' => [1,2,4]],
            ['week' => [1]],
            ['month' => [1,2,3,4,6]]
        ];
        $i = 0;
        foreach ($content->Data as $day)
        {
            $ohlc[$i][0] = ($day->time*1000);
            $ohlc[$i][1] = $day->open;
            $ohlc[$i][2] = $day->high;
            $ohlc[$i][3] = $day->low;
            $ohlc[$i][4] = $day->close;
            $volume[$i][0] = ($day->time*1000);
            $volume[$i][1] = $day->volumeto;
            $i++;
        }
        $response = json_encode(collect([
            'ohlc' => $ohlc,
            'volume' => $volume,
            'length' => $length,
            'groupingUnits' => $groupingUnits
        ]),JSON_PRETTY_PRINT);
        return $response;
    }


    public function cryptoCompareDaily($coin, $priceIn)
    {
        $url = 'https://min-api.cryptocompare.com/data/histoday?fsym='.$coin.'&tsym='.$priceIn.'&limit=2000&aggregate=3&e=CCCAGG';
        $data = Curl::to($url)
            ->returnResponseObject()
            ->get();

        $content = json_decode($data->content);
        $ohlc = [];
        $volume = [];
        $length = count($content->Data);
        $groupingUnits = [
            ['week' => [1]],
            ['month' => [1,2,3,4,6]]
        ];
        $i = 0;
        foreach ($content->Data as $day)
        {
            $ohlc[$i][0] = ($day->time*1000);
            $ohlc[$i][1] = $day->open;
            $ohlc[$i][2] = $day->high;
            $ohlc[$i][3] = $day->low;
            $ohlc[$i][4] = $day->close;
            $volume[$i][0] = ($day->time*1000);
            $volume[$i][1] = $day->volumeto;
            $i++;
        }
        $response = json_encode(collect([
            'ohlc' => $ohlc,
            'volume' => $volume,
            'length' => $length,
            'groupingUnits' => $groupingUnits
        ]),JSON_PRETTY_PRINT);
        return $response;
    }

}
