<?php

namespace App\Http\Controllers;

use App\BinanceApi;
use App\CoinbaseApi;
use App\GdaxApi;
use Auth;
use Illuminate\Http\Request;

class UsersApiController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getApis()
    {
        $apis = [];
        $gdaxApi = GdaxApi::where('user_id', '=', Auth::user()->id)->first();
        $binanceApi = BinanceApi::where('user_id', '=', Auth::user()->id)->first();
        $apis['gdax'] = $gdaxApi;
        $apis['binance'] = $binanceApi;
        return response()->json($apis);
    }

    public function getGdax()
    {
        $gdaxApi = GdaxApi::where('user_id', '=', Auth::user()->id)->first();
        return response()->json($gdaxApi);
    }


    public function storeGdax(Request $request)
    {
        $this->validate($request, [
            'passphrase' => 'required|min:3',
            'key' => 'required|min:3',
            'secret' => 'required|min:3'
        ]);

        $gdax = GdaxApi::create([
            'user_id' => Auth::user()->id,
            'api_key_passphrase' => trim($request->passphrase),
            'api_key' => trim($request->key),
            'api_key_secret' => trim($request->secret)
        ]);

        return redirect('/settings/api');
    }

    public function destroyGdax($id)
    {
        $gdaxApi = GdaxApi::find($id);
        $gdaxApi->delete();
        return redirect('/settings/api');
    }

    public function getBinance()
    {
        $binanceApi = BinanceApi::where('user_id', '=', Auth::user()->id)->first();
        return response()->json($binanceApi);
    }

    public function storeBinance(Request $request)
    {
        $this->validate($request, [
            'key' => 'required|min:3',
            'secret' => 'required|min:3'
        ]);

        $binance = BinanceApi::create([
            'user_id' => Auth::user()->id,
            'api_key' => trim($request->key),
            'api_key_secret' => trim($request->secret)
        ]);

        return redirect('/settings/api');
    }

    public function destroyBinance($id)
    {
        $binanceApi = BinanceApi::find($id);
        $binanceApi->delete();
        return redirect('/settings/api');
    }

    public function getBittrex()
    {
        //
    }

    public function storeBittrex()
    {
        //
    }

    public function destroyBittrex()
    {
        //
    }

    public function destroyCoinbase($id)
    {
        $coinbaseApi = CoinbaseApi::find($id);
        $coinbaseApi->delete();
        return redirect('/settings/api');
    }
}
