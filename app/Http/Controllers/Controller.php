<?php

namespace App\Http\Controllers;

use App\Coin;
use App\Post;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index() {
        return view('welcome');
    }

    public function getTicker()
    {
        $coins = Coin::whereIn('symbol', [
            'BTC',
            'ETH',
            'LTC',
            'MIOTA'
        ])
        ->get();
        return $coins->toJson();
    }

    public function sitemap()
    {
        $posts = Post::get();
        return response()->view('xml.sitemap', compact('posts'))->header('Content-Type', 'text/xml');
    }
}
