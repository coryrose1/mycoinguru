<?php
/*
|--------------------------------------------------------------------------
| Helpers
|--------------------------------------------------------------------------
*/
function usd($amount)
{
    return money_format('%.2n', $amount);
}

function localDateTime($date, $timezone) {
    $date = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date, 'UTC');
    $date->setTimezone($timezone);
    return $date->toDateTimeString();
}

function blogExcerpt($body, $slug) {
    if (strlen($body) >= 250)
    {
        $excerpt = mb_strimwidth($body, 0, 250, '...');
        $url = '/blog/'.$slug;
        $link = '<br><small><a href="'.$url.'">Read more</a><small>';
        return $excerpt.$link;
    } else {
        return $body;
    }

}

function array_date_sort($a, $b)
{
    $atime = \Carbon\Carbon::createFromTimestamp($a['time'])->timestamp;
//    dd(\Carbon\Carbon::createFromTimestamp($a['time'])->timestamp);
    $btime = \Carbon\Carbon::createFromTimestamp($b['time'])->timestamp;
    return $atime - $btime;
}