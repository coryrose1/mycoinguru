<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    protected $table = 'sales';

    protected $fillable = [
        'user_id',
        'coin_symbol',
        'amount',
        'usd',
        'fees',
        'total',
        'profit',
        'avg_cost',
        'date'
    ];

    protected $casts = [
        'amount' => 'float',
        'usd' => 'float',
        'fees' => 'float',
        'avg_cost' => 'float',
        'profit' => 'float',
        'total' => 'float',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id', 'users');
    }

    public function coin()
    {
        return $this->belongsTo(Coin::class, 'coin_symbol', 'symbol', 'coins');
    }

    public function gdaxImport()
    {
        return $this->hasOne(GdaxImport::class, 'sale_id', 'id');
    }

    public function coinbaseImport()
    {
        return $this->hasOne(CoinbaseImport::class, 'transaction_id', 'id');
    }
}
