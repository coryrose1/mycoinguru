<?php

namespace App\SlackCommandHandlers;

use Spatie\SlashCommand\Request;
use Spatie\SlashCommand\Response;
use Spatie\SlashCommand\Handlers\BaseHandler;

class Quote extends BaseHandler
{
public function canHandle(Request $request): bool
{
return starts_with($request->text, 'guru');
}

public function handle(Request $request): Response
{
return $this->respondToSlack("All is well with he who hodls until he grows odls.");
}
}