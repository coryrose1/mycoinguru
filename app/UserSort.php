<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSort extends Model
{
    protected $table = 'users_sort';

    protected $fillable = ['user_id', 'view', 'card_order', 'card_sort'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
