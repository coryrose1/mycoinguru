<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BinanceImport extends Model
{
    protected $table = 'binance_imports';

    protected $fillable = [
        'user_id',
        'binance_order_id',
        'binance_client_order_id',
        'coin_1',
        'coin_2',
        'amount',
        'total',
        'price',
        'date',
        'action',
        'fee',
        'imported',
        'trade_id'
    ];

    protected $casts = [
        'quantity' => 'float',
        'price' => 'float',
    ];
}
