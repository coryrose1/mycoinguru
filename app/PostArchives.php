<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostArchives extends Model
{
    protected $table = 'posts_archives';

    protected $fillable = ['month', 'posts'];
}
