<?php

namespace App\Console\Commands;

use App\Coin;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ClearOldCoins extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'coins:clear-old';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove coins that have not been updated in a month';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $coins = Coin::where('updated_at', '<', Carbon::now()->subMonth())->whereNotIn('coin_symbol', ['VEN'])->delete();
    }
}
