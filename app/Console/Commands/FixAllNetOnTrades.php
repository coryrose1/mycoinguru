<?php

namespace App\Console\Commands;

use App\Jobs\FixNetAtTrade;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;

class FixAllNetOnTrades extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'trades:fix-all-nets';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fix all the net at trade for each user via queue. Big command';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::whereHas('trades')->with('trades')->get();
        foreach ($users as $user)
        {
            $trades = $user->trades->sortBy('date');
            $seconds = 5;
            foreach ($trades as $trade){
                FixNetAtTrade::dispatch($trade)->delay(Carbon::now()->addSeconds($seconds));
                $seconds += 2;
            }
        }
    }
}
