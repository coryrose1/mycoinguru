<?php

namespace App\Console\Commands;

use App\Coin;
use Illuminate\Database\QueryException;
use Ixudra\Curl\Facades\Curl;
use Illuminate\Console\Command;

class UpdateCoinPrice extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'coins:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update coin pricing';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $response = Curl::to('https://api.coinmarketcap.com/v1/ticker/?limit=0')
            ->withContentType('application/json')
            ->returnResponseObject()
            ->get();
        $coins = collect(json_decode($response->content));
        $uniqueCoins = $coins->unique('symbol');
        foreach ($uniqueCoins as $coin)
        {
            try {
                $updateCoin = Coin::updateOrCreate([
                    'symbol' => $coin->symbol
                ],
                    [

                        'name' => $coin->name,
                        'rank' => $coin->rank,
                        'usd' => $coin->price_usd,
                        'btc' => $coin->price_btc,
                        'volume_24h_usd' => $coin->{'24h_volume_usd'},
                        'market_cap_usd' => $coin->market_cap_usd,
                        'available_supply' => $coin->available_supply,
                        'total_supply' => $coin->total_supply,
                        'max_supply' => $coin->max_supply,
                        'percent_change_1h' => $coin->percent_change_1h,
                        'percent_change_24h' => $coin->percent_change_24h,
                        'percent_change_7d' => $coin->percent_change_7d,
                        'cmc_id' => $coin->id
                    ]);

                if ((float)$updateCoin->usd > (float)$updateCoin->ath_usd || is_null($updateCoin->ath_usd))
                {
                    $updateCoin->ath_usd = $updateCoin->usd;
                    $updateCoin->save();
                }
            } catch(QueryException $e)
            {

            }
        }
    }
}
