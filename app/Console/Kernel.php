<?php

namespace App\Console;

use App\Console\Commands\ClearOldCoins;
use App\Console\Commands\FixAllNetOnTrades;
use App\Console\Commands\FixTaggablePosts;
use App\Console\Commands\UpdateCoinPrice;
use App\Jobs\UpdateCoins;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Spatie\Backup\Tasks\Backup\BackupJob;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        UpdateCoinPrice::class,
        ClearOldCoins::class,
        FixAllNetOnTrades::class,
        FixTaggablePosts::class,
        \Alfheim\CriticalCss\Console\CriticalCssMake::class,
        \Alfheim\CriticalCss\Console\CriticalCssClear::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
         $schedule->job(new UpdateCoins())
                  ->everyMinute();
        $schedule->command('wordpress-to-laravel:import -F -T')
            ->everyFifteenMinutes();
        $schedule->command('posts:fix-tags')
            ->everyFifteenMinutes();
         $schedule->command('coins:clear-old')
             ->daily();
        $schedule->command('backup:run')->daily()->at('05:00');
        $schedule->command('backup:clean')->daily()->at('05:05');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
