<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Holdings extends Model
{
    protected $table = 'holdings';

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function coin()
    {
        return $this->belongsTo(Coin::class, 'coin_symbol', 'symbol');
    }

    // $$ Invested into coin
    public function investment()
    {
        return $this->amount * $this->usd;
    }

    public function profit()
    {
        $investment = $this->investment();
        if ($investment > 0)
        {
            return ($this->amount * $this->coin->usd) - $this->investment();
        } else {
            return 0;
        }

    }

    // $$ Invested + Profit
    public function revenue()
    {
        return $this->amount * $this->coin->usd;
    }

    public function roi()
    {
        $investment = $this->investment();
        if ($investment > 0)
        {
            return (float)round(($this->profit() / $investment) * 100, 2);
        } else {
            return 0;
        }

    }
    protected $casts = [
        'amount' => 'float',
        'usd' => 'float',
        'revenue' => 'float',
    ];

}
