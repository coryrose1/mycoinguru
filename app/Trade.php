<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trade extends Model
{
    protected $table = 'trades';

    protected $fillable = [
        'user_id',
        'traded_coin',
        'received_coin',
        'traded_amount',
        'received_amount',
        'net_at_trade_usd',
        'date',
        'btc_at_trade',
        'fees_btc',
        'fees_usd',
        'btc_ratio'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    public function transactions()
    {
        return $this->hasMany(Transaction::class, 'trade_id', 'id');
    }

    public function tradedCoin()
    {
        return $this->belongsTo(Coin::class, 'traded_coin', 'symbol');
    }

    public function receivedCoin()
    {
        return $this->belongsTo(Coin::class, 'received_coin', 'symbol');
    }

    public function gdaxImport()
    {
        return $this->hasOne(GdaxImport::class, 'trade_id', 'id');
    }

    public function binanceImport()
    {
        return $this->hasOne(BinanceImport::class, 'trade_id', 'id');
    }

    protected $casts = [
        'traded_amount' => 'float',
        'received_amount' => 'float',
        'net_at_trade_usd' => 'float',
        'fees_usd' => 'float',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'date'
    ];
}
