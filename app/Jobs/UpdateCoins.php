<?php

namespace App\Jobs;

use App\Coin;
use Illuminate\Bus\Queueable;
use Illuminate\Database\QueryException;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use Ixudra\Curl\Facades\Curl;

class UpdateCoins implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $response = Curl::to('https://api.coinmarketcap.com/v1/ticker/?limit=0')
            ->withContentType('application/json')
            ->returnResponseObject()
            ->get();
        $coins = collect(json_decode($response->content));
        foreach ($coins as $coin)
        {
            try {
                $coin = Coin::updateOrCreate([
                    'symbol' => $coin->symbol,
                    'name' => $coin->name],
                    [
                        'rank' => $coin->rank,
                        'usd' => $coin->price_usd,
                        'btc' => $coin->price_btc,
                        'volume_24h_usd' => $coin->{'24h_volume_usd'},
                        'market_cap_usd' => $coin->market_cap_usd,
                        'available_supply' => $coin->available_supply,
                        'total_supply' => $coin->total_supply,
                        'max_supply' => $coin->max_supply,
                        'percent_change_1h' => $coin->percent_change_1h,
                        'percent_change_24h' => $coin->percent_change_24h,
                        'percent_change_7d' => $coin->percent_change_7d,
                        'cmc_id' => $coin->id
                    ]);

                if ((float)$coin->usd > (float)$coin->ath_usd || is_null($coin->ath_usd))
                {
                    $coin->ath_usd = $coin->usd;
                    $coin->save();
                }
            } catch(QueryException $e)
            {
                Log::debug($e);
            }
        }
    }
}
