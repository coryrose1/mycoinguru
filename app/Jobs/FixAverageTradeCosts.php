<?php

namespace App\Jobs;

use App\Trade;
use App\Transaction;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class FixAverageTradeCosts implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $transaction;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($transaction)
    {
        $this->transaction = $transaction;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Get every trade where date is after buy
        $tradesAfterBuy = Transaction::where('user_id', '=', $this->transaction->user_id)
            ->where('date', '>', $this->transaction->date)
            ->where('coin_symbol', '=', $this->transaction->coin_symbol)
            ->where('amount', '<', 0)
            ->whereNotNull('trade_id')
            ->get();

        // For every trade, get every add before trade and get avg cost of coin
        if ($tradesAfterBuy)
        {
            foreach ($tradesAfterBuy as $trade)
            {
                $beforeTransactions = Transaction::where('user_id', '=', $trade->user_id)
                    ->where('coin_symbol', '=', $trade->coin_symbol)
                    ->where('date', '<', $trade->date)
                    ->get();
                $sum = 0;
                $amount = 0;
                foreach ($beforeTransactions as $before)
                {
                    $sum += ($before->amount * $before->usd);
                    $amount += $before->amount;
                }

                if ($amount > 0)
                {
                    $buyAvg = $sum / $amount;
                } else {
                    $buyAvg = 0;
                }

                $trade->usd = $buyAvg;
                $trade->save();
            }
        }
    }
}
