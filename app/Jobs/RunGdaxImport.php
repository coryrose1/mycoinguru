<?php

namespace App\Jobs;

use App\GdaxImport;
use App\Sale;
use App\Trade;
use App\User;
use Carbon\Carbon;
use Curl;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Transaction;

class RunGdaxImport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $user;
    protected $import_id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user, $import_id)
    {
        $this->user = $user;
        $this->import_id = $import_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $importRow = GdaxImport::find($this->import_id);
        $mysql_date = Carbon::parse($importRow->date)->toDateTimeString();

        if ($importRow->action == 'buy')
        {
            //BUY
            $buy = Transaction::create([
                'user_id' => $this->user->id,
                'coin_symbol' => $importRow->coin_1,
                'amount' => $importRow->amount,
                'usd' => $importRow->usd,
                'date' => $mysql_date,
                'fees_usd' => $importRow->fees_usd,
            ]);
            $importRow->imported = 1;
            $importRow->transaction_id = $buy->id;
            $importRow->save();
            FixAverageTradeCosts::dispatch($buy);
            FixAverageSaleCosts::dispatch($buy->user_id, $buy->coin_symbol, $buy->date)->delay(Carbon::now()->addSeconds(1));
        } elseif ($importRow->action == 'sell')
        {
            $sold_avg_cost = $this->getAvgTradeCost($importRow->coin_1, $mysql_date);
            $profit = ($importRow->usd * $importRow->amount) - ($sold_avg_cost * $importRow->amount);
            //SELL
            $sell = Sale::create([
                'user_id' => $this->user->id,
                'coin_symbol' => $importRow->coin_1,
                'amount' => $importRow->amount,
                'usd' => $importRow->usd,
                'date' => $mysql_date,
                'fees' => $importRow->fees_usd,
                'total' => $importRow->amount * $importRow->usd,
                'profit' => $profit,
                'avg_cost' => $sold_avg_cost
            ]);

            $importRow->imported = 1;
            $importRow->sale_id = $sell->id;
            $importRow->save();
        } else
        {
            //TRADE
            // get coin 1 avg cost, and cost at trade
            $traded_avg_cost = $this->getAvgTradeCost($importRow->coin_1, $importRow->date);

            $parse = Carbon::parse($importRow->date);
            $datetime = Carbon::createFromFormat('Y-m-d H:i:s', $parse, $this->user->timezone);
            $datetime = $datetime->timezone('GMT');
            $start = $datetime->format('Y-m-d\TH:i:s\Z');
            $end = $datetime->addMinutes(10)->format('Y-m-d\TH:i:s\Z');
            $granularity = (int)60;

            $url = 'https://api.gdax.com/products/'.$importRow->coin_1.'-USD/candles/?start='.$start.'&end='.$end.'&granularity='.$granularity;
            $response = Curl::to($url)
                ->withContentType('application/json')
                ->withHeader('User-Agent: Test')
                ->returnResponseObject()
                ->get();

            $traded_cost = json_decode($response->content);
            $i = 0;
            $sum = 0;
//                dd($ethereum_history);
            foreach ($traded_cost as $unit)
            {
                // Average the low and high price and add to sum
                $average = ($unit[1] + $unit[2])/2;
                $sum += $average;
                $i++;
            }

            $traded_cost_at_trade = $sum/$i;
            $net_at_trade_usd = ($importRow->traded_amount * $traded_cost_at_trade) - ($importRow->traded_amount * $traded_avg_cost);

            $price_ratio = $importRow->received_amount / $importRow->traded_amount;
            $received_usd = $traded_cost_at_trade / $price_ratio;

            $trade = Trade::create([
                'user_id' => $this->user->id,
                'traded_coin' => $importRow->coin_1,
                'received_coin' => $importRow->coin_2,
                'traded_amount' => $importRow->traded_amount,
                'received_amount' => $importRow->received_amount,
                'date' => $mysql_date,
                'btc_at_trade' => $traded_cost_at_trade,
                'btc_ratio' => $price_ratio,
                'net_at_trade_usd' => $net_at_trade_usd
            ]);

            $traded = Transaction::create([
                'user_id' => $this->user->id,
                'coin_symbol' => $importRow->coin_1,
                'amount' => (-1 * $importRow->traded_amount),
                'usd' => $traded_avg_cost,
                'date' => $mysql_date,
                'trade_id' => $trade->id
            ]);

            $received = Transaction::create([
                'user_id' => $this->user->id,
                'coin_symbol' => $importRow->coin_2,
                'amount' => $importRow->received_amount,
                'usd' => $received_usd,
                'date' => $mysql_date,
                'trade_id' => $trade->id
            ]);

            $importRow->imported = 1;
            $importRow->trade_id = $trade->id;
            $importRow->save();
            FixAverageTradeCosts::dispatch($received);
            FixAverageSaleCosts::dispatch($received->user_id, $received->coin_symbol, $received->date)->delay(Carbon::now()->addSeconds(1));

        }
    }

    public function getAvgTradeCost($coin_symbol, $date)
    {
        $transactions = Transaction::where('coin_symbol', '=', $coin_symbol)
            ->where('date', '<=', $date)
            ->where('user_id', '=', $this->user->id)
            ->where('amount', '>', 0)
            ->get();

        $sum = 0;
        $amount = 0;
        foreach ($transactions as $before)
        {
            $sum += ($before->amount * $before->usd);
            $amount += $before->amount;
        }

        if ($amount > 0)
        {
            $buyAvg = $sum / $amount;
        } else {
            $buyAvg = 0;
        }
        return (float)$buyAvg;

    }
}
