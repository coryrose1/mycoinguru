<?php

namespace App\Jobs;

use App\Sale;
use App\Transaction;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class FixAverageSaleCosts implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $user_id;
    protected $coin_symbol;
    protected $date;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user_id, $coin_symbol, $date)
    {
        $this->user_id = $user_id;
        $this->coin_symbol = $coin_symbol;
        $this->date = $date;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $salesAfterDate = Sale::where('user_id', '=', $this->user_id)
            ->where('coin_symbol', '=', $this->coin_symbol)
            ->where('date', '>', $this->date)
            ->get();

        if ($salesAfterDate)
        {
            foreach ($salesAfterDate as $sale)
            {
                $beforeTransactions = Transaction::where('user_id', '=', $sale->user_id)
                    ->where('coin_symbol', '=', $sale->coin_symbol)
                    ->where('date', '<', $sale->date)
                    ->get();
                $sum = 0;
                $amount = 0;
                foreach ($beforeTransactions as $before)
                {
                    $sum += ($before->amount * $before->usd);
                    $amount += $before->amount;
                }
                if ($amount > 0)
                {
                    $buyAvg = $sum / $amount;
                } else {
                    $buyAvg = 0;
                }
                $sale->avg_cost = $buyAvg;
                $sale->profit = ($sale->amount * $sale->usd) - ($sale->amount * $buyAvg);
                $sale->save();
            }
        }
    }
}
