<?php

namespace App\Jobs;

use App\CoinbaseImport;
use App\Sale;
use Carbon\Carbon;
use App\Transaction;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class RunCoinbaseImport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $import_id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($import_id)
    {
        $this->import_id = $import_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $import = CoinbaseImport::find($this->import_id);
        if ($import->action == 'buy')
        {
            $buy = Transaction::create([
                'user_id' => $import->user_id,
                'coin_symbol' => $import->coin_symbol,
                'amount' => $import->amount,
                'usd' => $import->usd,
                'date' => $import->date,
                'fees_usd' => $import->fees_usd,
            ]);
            FixAverageTradeCosts::dispatch($buy);
            FixAverageSaleCosts::dispatch($buy->user_id, $buy->coin_symbol, $buy->date)->delay(Carbon::now()->addSeconds(1));
        } elseif ($import->action == 'sell')
        {
            // We have to determine avg cost at time of sale for profit
            $sold_avg_cost = $this->getAvgTradeCost($import->coin_1, $import->date, $import->user_id);
            $profit = ($import->usd * $import->amount) - ($sold_avg_cost * $import->amount);
            $sale = Sale::create([
                'user_id' => $import->user_id,
                'coin_symbol' => $import->coin_symbol,
                'amount' => $import->amount,
                'usd' => $import->usd,
                'date' => $import->date,
                'fees' => $import->fees_usd,
                'total' => (float)($import->usd * $import->amount),
                'profit' => $profit,
                'avg_cost' => $sold_avg_cost
            ]);
        }

        $import->imported = 1;
        $import->save();

    }

    public function getAvgTradeCost($coin_symbol, $date, $user_id)
    {
        $transactions = Transaction::where('coin_symbol', '=', $coin_symbol)
            ->where('date', '<=', $date)
            ->where('user_id', '=', $user_id)
            ->where('amount', '>', 0)
            ->get();

        $sum = 0;
        $amount = 0;
        foreach ($transactions as $before)
        {
            $sum += ($before->amount * $before->usd);
            $amount += $before->amount;
        }

        if ($amount > 0)
        {
            $buyAvg = $sum / $amount;
        } else {
            $buyAvg = 0;
        }
        return (float)$buyAvg;

    }
}
