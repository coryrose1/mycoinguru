<?php

namespace App\Jobs;

use App\Trade;
use App\Transaction;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class FixNetAtTrade implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $trade;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Trade $trade)
    {
        $this->trade = $trade;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $beforeTransactions = Transaction::where('user_id', '=', $this->trade->user_id)
            ->where('coin_symbol', '=', $this->trade->traded_coin)
            ->where('date', '<=', $this->trade->date)
            ->where('amount', '>', 0)
            ->get();
        $sum = 0;
        $amount = 0;
        foreach ($beforeTransactions as $before)
        {
            $sum += ($before->amount * $before->usd);
            $amount += $before->amount;
        }

        if ($amount > 0)
        {
            $tradedAvgCost = $sum / $amount;
        } else {
            $tradedAvgCost = 0;
        }

        if ($this->trade->traded_coin == 'BTC' || $this->trade->traded_coin == 'ETH')
        {
            $netAtTrade = ($this->trade->btc_at_trade * $this->trade->traded_amount) - ($tradedAvgCost * $this->trade->traded_amount);
        } else {
            $coinAtTrade = $this->trade->btc_at_trade * $this->trade->btc_ratio;
            $netAtTrade = ($coinAtTrade * $this->trade->traded_amount) - ($tradedAvgCost * $this->trade->traded_amount);
        }
        $newTrade = Trade::find($this->trade->id);
        $newTrade->net_at_trade_usd = (float)$netAtTrade;
        $newTrade->save();
    }
}
