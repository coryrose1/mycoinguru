<?php

namespace App\Jobs;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Curl;
use App\Jobs\FixAverageTradeCosts;
use App\Trade;
use App\BinanceImport;
use App\Transaction;
use Carbon\Carbon;

class RunBinanceImport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $user;
    protected $import;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user, $import)
    {
        $this->user = $user;
        $this->import = $import;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $importRow = BinanceImport::find($this->import['id']);
        $mysql_date = Carbon::parse($this->import['date'])->toDateTimeString();
        $parse = Carbon::parse($this->import['date']);
        $datetime = Carbon::createFromFormat('Y-m-d H:i:s', $parse, $this->user->timezone);
        $datetime = $datetime->timezone('GMT');
        $time_gmt = $datetime->toDateTimeString();
        if ($this->import['coin_1'] == 'ETH')
        {
            // Get ETH value at trade
            $start = $datetime->format('Y-m-d\TH:i:s\Z');
            $end = $datetime->addMinutes(10)->format('Y-m-d\TH:i:s\Z');
            $granularity = (int)60;

            $url = 'https://api.pro.coinbase.com/products/ETH-USD/candles/?start='.$start.'&end='.$end.'&granularity='.$granularity;
            $response = Curl::to($url)
                ->withContentType('application/json')
                ->withHeader('User-Agent: Test')
                ->returnResponseObject()
                ->get();

            $ethereum_history = json_decode($response->content);
            $i = 0;
            $sum = 0;
//                dd($ethereum_history);
            foreach ($ethereum_history as $unit)
            {
                // Average the low and high price and add to sum
                $average = ($unit[1] + $unit[2])/2;
                $sum += $average;
                $i++;
            }

            $eth_historical_price = $sum/$i;

            if ($this->import['action'] == 'BUY')
            {
                $received_usd = $eth_historical_price * $this->import['price'];
                $received_amount = (float)($this->import['amount'] * 0.999);
                $traded_usd = $this->getAvgTradeCost('ETH', $this->import['date']);
                $net_at_trade_usd = ($eth_historical_price * $this->import['total']) - ($traded_usd * $this->import['total']);
                // Create trade row
                $trade = Trade::create([
                    'user_id' => $this->user->id,
                    'traded_coin' => 'ETH',
                    'received_coin' => $this->import['coin_2'],
                    'traded_amount' => $this->import['total'],
                    'received_amount' => $received_amount,
                    'date' => $mysql_date,
                    'btc_at_trade' => $eth_historical_price,
                    'btc_ratio' => $this->import['price'],
                    'net_at_trade_usd' => $net_at_trade_usd
                ]);
                // transaction 1 is minus ETH row
                $traded = Transaction::create([
                    'user_id' => $this->user->id,
                    'coin_symbol' => 'ETH',
                    'amount' => (-1 * $this->import['total']),
                    'usd' => $traded_usd,
                    'date' => $mysql_date,
                    'trade_id' => $trade->id
                ]);
                // transaction 2 is plus coin_2 row
                $received = Transaction::create([
                    'user_id' => $this->user->id,
                    'coin_symbol' => $this->import['coin_2'],
                    'amount' => $received_amount,
                    'usd' => $received_usd,
                    'date' => $mysql_date,
                    'trade_id' => $trade->id
                ]);
                // Fix average trade costs
                $importRow->imported = 1;
                $importRow->trade_id = $trade->id;
                $importRow->save();
                FixAverageTradeCosts::dispatch($received)->delay(Carbon::now()->addSeconds(1));
                FixAverageSaleCosts::dispatch($received->user_id, $received->coin_symbol, $received->date)->delay(Carbon::now()->addSeconds(2));
            } elseif ($this->import['action'] == 'SELL')
            {
                $traded_usd = $this->getAvgTradeCost($this->import['coin_2'], $this->import['date']);
                $received_amount = (float)($this->import['total'] * 0.999);
                $net_at_trade_usd = (($eth_historical_price * $this->import['price']) * $this->import['amount']) - ($traded_usd * $this->import['amount']);
                // Create trade row
                $trade = Trade::create([
                    'user_id' => $this->user->id,
                    'traded_coin' => $this->import['coin_2'],
                    'received_coin' => 'ETH',
                    'traded_amount' => $this->import['amount'],
                    'received_amount' => $received_amount,
                    'date' => $mysql_date,
                    'btc_at_trade' => $eth_historical_price,
                    'btc_ratio' => $this->import['price'],
                    'net_at_trade_usd' => $net_at_trade_usd
                ]);
                // transaction 1 is minus ETH row
                $traded = Transaction::create([
                    'user_id' => $this->user->id,
                    'coin_symbol' => $this->import['coin_2'],
                    'amount' => (-1 * $this->import['amount']),
                    'usd' => $traded_usd,
                    'date' => $mysql_date,
                    'trade_id' => $trade->id
                ]);
                // transaction 2 is plus coin_2 row
                $received = Transaction::create([
                    'user_id' => $this->user->id,
                    'coin_symbol' => 'ETH',
                    'amount' => $received_amount,
                    'usd' => $eth_historical_price,
                    'date' => $mysql_date,
                    'trade_id' => $trade->id
                ]);
                // Fix average trade costs
                $importRow->imported = 1;
                $importRow->trade_id = $trade->id;
                $importRow->save();
                FixAverageTradeCosts::dispatch($received);
                FixAverageSaleCosts::dispatch($received->user_id, $received->coin_symbol, $received->date)->delay(Carbon::now()->addSeconds(1));
            }

        } elseif ($this->import['coin_1'] == 'BTC')
        {
            // Get BTC value at trade
            $start = $datetime->format('Y-m-d\TH:i:s\Z');
            $end = $datetime->addMinutes(10)->format('Y-m-d\TH:i:s\Z');
            $granularity = (int)60;

            $url = 'https://api.pro.coinbase.com/products/BTC-USD/candles/?start='.$start.'&end='.$end.'&granularity='.$granularity;
            $response = Curl::to($url)
                ->withContentType('application/json')
                ->withHeader('User-Agent: Test')
                ->returnResponseObject()
                ->get();

            $btc_history = json_decode($response->content);
            $i = 0;
            $sum = 0;
            foreach ($btc_history as $unit)
            {
                // Average the low and high price and add to sum
                $average = ($unit[1] + $unit[2])/2;
                $sum += $average;
                $i++;
            }
            $btc_historical_price = $sum/$i;

            if ($this->import['action'] == 'BUY')
            {
                $received_usd = $btc_historical_price * $this->import['price'];
                $received_amount = (float)($this->import['amount'] * 0.999);
                $traded_usd = $this->getAvgTradeCost('BTC', $this->import['date']);
                $net_at_trade_usd = ($btc_historical_price * $this->import['total']) - ($traded_usd * $this->import['total']);
                // Create trade row
                $trade = Trade::create([
                    'user_id' => $this->user->id,
                    'traded_coin' => 'BTC',
                    'received_coin' => $this->import['coin_2'],
                    'traded_amount' => $this->import['total'],
                    'received_amount' => $received_amount,
                    'date' => $mysql_date,
                    'btc_at_trade' => $btc_historical_price,
                    'btc_ratio' => $this->import['price'],
                    'net_at_trade_usd' => $net_at_trade_usd
                ]);
                // transaction 1 is minus ETH row
                $traded = Transaction::create([
                    'user_id' => $this->user->id,
                    'coin_symbol' => 'BTC',
                    'amount' => (-1 * $this->import['total']),
                    'usd' => $traded_usd,
                    'date' => $mysql_date,
                    'trade_id' => $trade->id
                ]);
                // transaction 2 is plus coin_2 row
                $received = Transaction::create([
                    'user_id' => $this->user->id,
                    'coin_symbol' => $this->import['coin_2'],
                    'amount' => $received_amount,
                    'usd' => $received_usd,
                    'date' => $mysql_date,
                    'trade_id' => $trade->id
                ]);
                // Fix average trade costs
                $importRow->imported = 1;
                $importRow->trade_id = $trade->id;
                $importRow->save();
                FixAverageTradeCosts::dispatch($received);
                FixAverageSaleCosts::dispatch($received->user_id, $received->coin_symbol, $received->date)->delay(Carbon::now()->addSeconds(1));
            } elseif ($this->import['action'] == 'SELL')
            {
                $traded_usd = $this->getAvgTradeCost($this->import['coin_2'], $this->import['date']);
                $received_amount = (float)($this->import['total'] * 0.999);
                $net_at_trade_usd = (($btc_historical_price * $this->import['price']) * $this->import['amount']) - ($traded_usd * $this->import['amount']);
                // Create trade row
                $trade = Trade::create([
                    'user_id' => $this->user->id,
                    'traded_coin' => $this->import['coin_2'],
                    'received_coin' => 'BTC',
                    'traded_amount' => $this->import['amount'],
                    'received_amount' => $received_amount,
                    'date' => $mysql_date,
                    'btc_at_trade' => $btc_historical_price,
                    'btc_ratio' => $this->import['price'],
                    'net_at_trade_usd' => $net_at_trade_usd
                ]);
                // transaction 1 is minus ETH row
                $traded = Transaction::create([
                    'user_id' => $this->user->id,
                    'coin_symbol' => $this->import['coin_2'],
                    'amount' => (-1 * $this->import['amount']),
                    'usd' => $traded_usd,
                    'date' => $mysql_date,
                    'trade_id' => $trade->id
                ]);
                // transaction 2 is plus coin_2 row
                $received = Transaction::create([
                    'user_id' => $this->user->id,
                    'coin_symbol' => 'BTC',
                    'amount' => $received_amount,
                    'usd' => $btc_historical_price,
                    'date' => $mysql_date,
                    'trade_id' => $trade->id
                ]);
                // Fix average trade costs
                $importRow->imported = 1;
                $importRow->trade_id = $trade->id;
                $importRow->save();
                FixAverageTradeCosts::dispatch($received);
                FixAverageSaleCosts::dispatch($received->user_id, $received->coin_symbol, $received->date)->delay(Carbon::now()->addSeconds(1));
            }

        }
    }

    public function getAvgTradeCost($coin_symbol, $date)
    {
        $transactions = Transaction::where('coin_symbol', '=', $coin_symbol)
            ->where('date', '<', $date)
            ->where('user_id', '=', $this->user->id)
            ->get();

        $sum = 0;
        $amount = 0;
        foreach ($transactions as $before)
        {
            $sum += ($before->amount * $before->usd);
            $amount += $before->amount;
        }

        if ($amount > 0)
        {
            $buyAvg = $sum / $amount;
        } else {
            $buyAvg = 0;
        }
        return (float)$buyAvg;

    }
}
