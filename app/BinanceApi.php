<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BinanceApi extends Model
{
    protected $table = 'binance_apis';

    protected $fillable = ['user_id', 'api_key', 'api_key_secret'];
}
