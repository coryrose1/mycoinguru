<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GdaxApi extends Model
{
    protected $table = 'gdax_apis';

    protected $fillable = [
        'user_id',
        'api_key',
        'api_key_secret',
        'api_key_passphrase'
    ];
}
