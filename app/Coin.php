<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coin extends Model
{
    protected $table = 'coins';

    protected $fillable = [
        'rank',
        'symbol',
        'name',
        'usd',
        'ath_usd',
        'btc',
        'volume_24h_usd',
        'market_cap_usd',
        'available_supply',
        'total_supply',
        'max_supply',
        'percent_change_1h',
        'percent_change_24h',
        'percent_change_7d',
        'cmc_id'
    ];

    public function transactions()
    {
        return $this->hasMany(Transaction::class, 'coin_symbol', 'symbol');
    }

    protected $casts = [
        'volume' => 'integer',
        'usd' => 'float',
        'ath_usd' => 'float',
        'btc' => 'float',
        'percent_change_1h' => 'float',
        'percent_change_24h' => 'float',
        'percent_change_7d' => 'float',
    ];
}
