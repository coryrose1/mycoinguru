<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Cashier\Billable;

class User extends Authenticatable
{
    use Notifiable, Billable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'timezone', 'dark_mode', 'hide_low_holdings', 'last_login'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function transactions()
    {
        return $this->hasMany(Transaction::class, 'user_id', 'id');
    }

    public function holdings()
    {
        return $this->hasMany(Holdings::class, 'user_id', 'id');
    }

    public function trades()
    {
        return $this->hasMany(Trade::class, 'user_id', 'id');
    }

    public function sales()
    {
        return $this->hasMany(Sale::class, 'user_id', 'id');
    }

    public function sort()
    {
        return $this->hasOne(UserSort::class, 'user_id', 'id');
    }

    public function watchlist()
    {
        return $this->belongsToMany(Coin::class, 'watchlists', 'user_id', 'coin_id');
    }

    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id', 'id', 'roles');
    }

    public function posts()
    {
        return $this->hasMany(Post::class, 'user_id', 'id');
    }

    public function isWriter()
    {
        if ($this->role_id > 1)
        {
            return true;
        }
        return false;
    }

    public function isAdmin()
    {
        if ($this->role_id > 2)
        {
            return true;
        }
        return false;
    }

    public function coinbaseApi()
    {
        return $this->hasOne(CoinbaseApi::class);
    }

    public function gdaxApi(){
        return $this->hasOne(GdaxApi::class);
    }

    public function binanceApi()
    {
        return $this->hasOne(BinanceApi::class);
    }

    public function profitOnCoin($coin_symbol)
    {
        foreach ($this->holdings as $coin)
        {
            if ($coin->coin->symbol == $coin_symbol)
            {
                if ($coin->amount == 0){
                    return 0;
                }
                $cost = ($coin->amount * $coin->usd);
                $value = ($coin->amount * $coin->coin->usd);
                $profit = $value - $cost;
                return $profit;
            }
        }
    }

    public function roiOnCoin($coin_symbol)
    {
        foreach ($this->holdings as $holding)
        {
            if ($holding->coin->symbol == $coin_symbol)
            {
                if ($holding->amount == 0){
                    return 0;
                }
                $cost = ($holding->amount * $holding->usd);
                $value = ($holding->amount * $holding->coin->usd);
                return (float)round(((($value / $cost)*100)-100),2);
            }
        }
    }

    public function currentROI()
    {
        return (float)round((($this->currentProfit() / $this->currentInvestment()) * 100), 2);
    }

    public function yearlyNetAtTrade()
    {
        $nets = [];
       foreach ($this->trades as $trade) {
           $nets[] = [
               'year' => Carbon::parse($trade->date)->year,
               'amount' => $trade->net_at_trade_usd
           ];
       }
        $nets = collect($nets);
        $nets = $nets->groupBy('year');
       foreach ($nets as $year)
       {
           $year->sum = $year->sum('amount');
       }
       return $nets;
    }

    public function holdingPercent($coin_symbol)
    {
        foreach ($this->holdings as $holding)
        {
            if ($holding->coin_symbol == $coin_symbol)
            {
                $percentage = (($holding->amount * $holding->coin->usd) / $this->revenue()) * 100;
            }
        }
        return $percentage;
    }

    public function originalInvestment()
    {
        $originalInvestment = 0;
        foreach ($this->transactions as $transaction)
        {
            if (!$transaction->trade_id)
            {
                $transactionCost = ($transaction->amount * $transaction->usd);
                $originalInvestment += $transactionCost;
            }
        }
        if ($originalInvestment - $this->salesTotal() < 0)
        {
            return 0;
        } else {
            return $originalInvestment - $this->salesTotal();
        }
    }

    public function salesAvgCost()
    {
        $salesAvgCost = 0;
        foreach ($this->sales as $sale)
        {
            $saleCost = ($sale->amount * $sale->avg_cost);
            $salesAvgCost += $saleCost;
        }
        return $salesAvgCost;
    }

    public function salesProfit()
    {
        $salesProfit = 0;
        foreach ($this->sales as $sale)
        {
            $salesProfit += $sale->profit;
        }
        return $salesProfit;
    }

    public function salesTotal()
    {
        $salesTotal = 0;
        foreach ($this->sales as $sale)
        {
            $salesTotal += ($sale->amount * $sale->usd);
        }
        return $salesTotal;
    }

    public function costBasis()
    {
        $costBasis = 0;
        foreach ($this->holdings as $holding)
        {
            $holding = ($holding->amount * $holding->usd);
            $costBasis += $holding;
        }
        return $costBasis;
    }

    public function unrealizedGains()
    {
        $unrealizedGains = 0;
        foreach ($this->holdings as $holding)
        {
            $holding = ($holding->amount * $holding->coin->usd) - ($holding->amount * $holding->usd);
            $unrealizedGains += $holding;
        }
        return $unrealizedGains;
    }

    public function revenue()
    {
        $revenue = $this->costBasis() + $this->salesProfit() + $this->unrealizedGains();
        return $revenue;
    }

    public function profit()
    {
        $profit = $this->revenue() - $this->costBasis();
        return $profit;
    }

    public function roi()
    {
        return (float)round((($this->profit() / $this->costBasis()) * 100), 2);
    }

    public function profitOnOriginalInvestment()
    {
        $profit = $this->revenue() - $this->originalInvestment();
        return $profit;
    }

    public function roiOnOriginalInvestment()
    {
        return (float)round((($this->revenue() / $this->originalInvestment()) * 100), 2);
    }

    public function totalNetAtTrade()
    {
        return $this->costBasis() - $this->originalInvestment();
    }


}
