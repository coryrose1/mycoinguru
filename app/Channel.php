<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Channel extends Model
{
    use Notifiable;

    protected $table = 'channels';

    protected $fillable = ['name', 'webhook'];

    public function routeNotificationForSlack() {
        return $this->webhook;
    }
}
