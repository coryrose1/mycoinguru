<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CoinbaseImport extends Model
{
    protected $table = 'coinbase_imports';

    protected $fillable = ['id', 'coinbase_id', 'user_id', 'coin_symbol', 'action', 'amount', 'usd', 'fees_usd', 'date', 'imported', 'transaction_id', 'sale_id'];

    protected $casts = [
        'amount' => 'float',
        'usd' => 'float',
        'fees_usd' => 'float',
    ];
}
