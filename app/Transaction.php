<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $table = 'transactions';

    protected $fillable = [
        'user_id',
        'coin_symbol',
        'amount',
        'usd',
        'btc',
        'eth',
        'date',
        'trade_id',
        'fees_usd'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id', 'users');
    }

    public function coin()
    {
        return $this->belongsTo(Coin::class, 'coin_symbol', 'symbol','coins');
    }

    public function profit()
    {
        return (($this->coin->USD * $this->amount) - ($this->price * $this->amount));
    }

    public function trade()
    {
        return $this->belongsTo(Trade::class, 'trade_id', 'id');
    }

    public function gdaxImport()
    {
        return $this->hasOne(GdaxImport::class, 'transaction_id', 'id');
    }

    public function coinbaseImport()
    {
        return $this->hasOne(CoinbaseImport::class, 'transaction_id', 'id');
    }

    protected $casts = [
        'amount' => 'float',
        'usd' => 'float',
        'fees_usd' => 'float',
    ];
}
