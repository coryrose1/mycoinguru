<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CoinbaseApi extends Model
{
    protected $table = 'coinbase_apis';

    protected $fillable = ['user_id', 'access_token', 'refresh_token', 'expires_at', 'scope'];
}
