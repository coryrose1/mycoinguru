<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBinanceImportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('binance_imports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('binance_order_id')->unsigned()->nullable();
            $table->string('binance_client_order_id')->nullable();
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')
                ->on('users')->onDelete('cascade');
            $table->string('coin_1');
            $table->foreign('coin_1')->references('symbol')
                ->on('coins')->onDelete('cascade');
            $table->string('coin_2');
            $table->foreign('coin_2')->references('symbol')
                ->on('coins')->onDelete('cascade');
            $table->string('action')->nullable();
            $table->decimal('amount',18,8);
            $table->decimal('total',18,8);
            $table->decimal('price',18,8);
            $table->decimal('fee',18,8);
            $table->dateTime('date');
            $table->boolean('imported')->default(0);
            $table->integer('trade_id')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('binance_imports');
    }
}
