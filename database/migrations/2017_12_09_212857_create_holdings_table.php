<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateHoldingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE VIEW `holdings` AS SELECT t.user_id, t.coin_symbol, (SUM(t.amount) - IFNULL(SUM(s.amount),0)) amount, (SUM(t.amount*t.usd) / SUM(t.amount)) usd
        FROM avg_transactions t
        LEFT JOIN avg_sales s on t.coin_symbol = s.coin_symbol
        AND t.user_id = s.user_id
        GROUP BY t.user_id, t.coin_symbol");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW `holdings`");
    }
}
