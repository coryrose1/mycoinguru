<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')
                ->on('users')->onDelete('cascade');
            $table->string('coin_symbol');
            $table->foreign('coin_symbol')->references('symbol')
                ->on('coins')->onDelete('cascade');
            $table->decimal('amount',18,8);
            $table->decimal('usd',18,8)->nullable();
            $table->decimal('fees',18,8)->nullable();
            $table->decimal('profit',18,8)->nullable();
            $table->decimal('total',18,8)->nullable();
            $table->decimal('avg_cost',18,8)->nullable();
            $table->dateTime('date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales');
    }
}
