<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsArchivesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE VIEW `posts_archives` AS SELECT DATE_FORMAT(published_at, '%Y-%m-01') month, count(*) posts
        FROM posts
        GROUP BY DATE_FORMAT(published_at, '%Y-%m')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW `posts_archives`");
    }
}
