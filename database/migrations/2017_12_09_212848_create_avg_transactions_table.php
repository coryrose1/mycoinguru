<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateAvgTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE VIEW `avg_transactions` AS SELECT user_id, coin_symbol, SUM(amount) amount, (SUM(amount*usd) / SUM(amount)) usd
        FROM transactions GROUP BY user_id, coin_symbol");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW `avg_transactions`");
    }
}
