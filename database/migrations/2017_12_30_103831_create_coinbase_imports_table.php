<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoinbaseImportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coinbase_imports', function (Blueprint $table) {
            $table->increments('id');
            $table->string('coinbase_id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')
                ->on('users')->onDelete('cascade');
            $table->string('coin_symbol');
            $table->foreign('coin_symbol')->references('symbol')
                ->on('coins')->onDelete('cascade');
            $table->string('action')->nullable();
            $table->decimal('amount',18,8);
            $table->decimal('usd',18,8);
            $table->decimal('fees_usd',18,8)->nullable();
            $table->dateTime('date');
            $table->boolean('imported')->default(0);
            $table->integer('transaction_id')->unsigned()->nullable();
            $table->integer('sale_id')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coinbase_imports');
    }
}
