<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')
                ->on('users')->onDelete('cascade');
            $table->string('coin_symbol');
            $table->foreign('coin_symbol')->references('symbol')
                ->on('coins')->onDelete('cascade');
            $table->decimal('amount',18,8);
            $table->decimal('usd',18,8)->nullable();
            $table->decimal('btc',18,8)->nullable();
            $table->decimal('eth',18,8)->nullable();
            $table->dateTime('date');
            $table->integer('trade_id')->unsigned()->nullable();
            $table->foreign('trade_id')->references('id')
                ->on('trades')->onDelete('cascade');
            $table->decimal('fees_usd',18,8)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
