<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGdaxImportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gdax_imports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('gdax_trade_id')->unsigned();
            $table->string('gdax_order_id', 45);
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')
                ->on('users')->onDelete('cascade');
            $table->string('coin_1')->nullable();
            $table->string('coin_2')->nullable();
            $table->string('action')->nullable();
            $table->decimal('amount',18,8);
            $table->decimal('usd',18,8);
            $table->decimal('fees_usd',18,8)->nullable();
            $table->decimal('traded_amount',18,8);
            $table->decimal('received_amount',18,8);
            $table->dateTime('date');
            $table->boolean('imported')->default(0);
            $table->integer('transaction_id')->unsigned()->nullable();
            $table->integer('trade_id')->unsigned()->nullable();
            $table->integer('sale_id')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gdax_imports');
    }
}
