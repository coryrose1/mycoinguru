<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTradesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trades', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')
                ->on('users')->onDelete('cascade');
            $table->string('traded_coin');
            $table->foreign('traded_coin')->references('symbol')
                ->on('coins')->onDelete('cascade');
            $table->string('received_coin');
            $table->foreign('received_coin')->references('symbol')
                ->on('coins')->onDelete('cascade');
            $table->decimal('traded_amount',18,8)->nullable();
            $table->decimal('received_amount',18,8)->nullable();
            $table->decimal('net_at_trade_usd',18,8)->nullable();
            $table->dateTime('date');
            $table->decimal('btc_at_trade',18,8)->nullable();
            $table->decimal('fees_btc',18,8)->nullable();
            $table->decimal('fees_usd',18,8)->nullable();
            $table->decimal('btc_ratio',18,8)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trades');
    }
}
