<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coins', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('rank')->unsigned();
            $table->string('symbol')->index();
            $table->string('name');
            $table->decimal('usd',16,6)->nullable();
            $table->decimal('btc',16,6)->nullable();
            $table->decimal('ath_usd',16,6)->nullable();
            $table->bigInteger('volume_24h_usd')->unsigned()->nullable();
            $table->bigInteger('market_cap_usd')->unsigned()->nullable();
            $table->bigInteger('available_supply')->unsigned()->nullable();
            $table->bigInteger('total_supply')->unsigned()->nullable();
            $table->bigInteger('max_supply')->unsigned()->nullable();
            $table->decimal('percent_change_1h',8,2)->nullable();
            $table->decimal('percent_change_24h',8,2)->nullable();
            $table->decimal('percent_change_7d',8,2)->nullable();
            $table->string('icon', 10);
            $table->string('cmc_id')->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coins');
    }
}
