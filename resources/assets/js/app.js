
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import Buefy from 'buefy';
import Vue2Filters from 'vue2-filters';
import VueSocketio from 'vue-socket.io';
import VueHighcharts from 'vue-highcharts';
import Highcharts from 'highcharts';
import loadStock from 'highcharts/modules/stock';
window.moment = require('moment');
moment().format();


Highcharts.setOptions({
    global: {
        useUTC: true
    }
});

loadStock(Highcharts);

Vue.use(VueHighcharts, { Highcharts });

// Vue.use(VueSocketio, 'https://coincap.io');
Vue.use(Vue2Filters);
Vue.use(Buefy, {
    defaultIconPack: 'fa'
});

/**
 * VUE COMPONENTS
 */

Vue.component('help-guide', require('./components/help/Help.vue').default);
Vue.component('watchlist', require('./components/Watchlist.vue').default);
Vue.component('coins', require('./components/coins/Coins.vue').default);
Vue.component('coins-table', require('./components/coins/CoinsTable.vue').default);
Vue.component('coin-icon', require('./components/components/CoinIcon.vue').default);
Vue.component('notification', require('./components/components/Notification.vue').default);
Vue.component('donate-eth-modal', require('./components/components/DonateEthModal.vue').default);
Vue.component('watchlist-modal', require('./components/components/WatchlistModal.vue').default);
Vue.component('confirm-delete-modal', require('./components/components/ConfirmDeleteModal.vue').default);
Vue.component('coin-search', require('./components/components/CoinSearch.vue').default);
Vue.component('dashboard', require('./components/dashboard/Dashboard.vue').default);
Vue.component('portfolio', require('./components/dashboard/portfolio/Portfolio.vue').default);
Vue.component('overview', require('./components/dashboard/portfolio/Overview.vue').default);
Vue.component('overview-card', require('./components/dashboard/portfolio/OverviewCard.vue').default);
Vue.component('overview-table', require('./components/dashboard/portfolio/OverviewTable.vue').default);
Vue.component('overview-table-fullscreen', require('./components/dashboard/portfolio/OverviewTableFullscreen.vue').default);
Vue.component('by-coin-detail', require('./components/dashboard/portfolio/ByCoinDetail.vue').default);
Vue.component('coin-chart', require('./components/dashboard/portfolio/CoinChart.vue').default);
Vue.component('transactions', require('./components/dashboard/portfolio/Transactions.vue').default);
Vue.component('breakdown', require('./components/dashboard/portfolio/Breakdown.vue').default);
Vue.component('history', require('./components/dashboard/portfolio/History.vue').default);
Vue.component('my-trades', require('./components/dashboard/portfolio/MyTrades.vue').default);
Vue.component('my-sales', require('./components/dashboard/portfolio/MySales.vue').default);
Vue.component('my-coins', require('./components/dashboard/MyCoins.vue').default);
Vue.component('add-coins', require('./components/dashboard/add-coins/AddCoins.vue').default);
Vue.component('usd-to-coin', require('./components/dashboard/add-coins/UsdToCoin.vue').default);
Vue.component('trade-coins', require('./components/dashboard/trade-coins/TradeCoins.vue').default);
Vue.component('coin-to-coin', require('./components/dashboard/trade-coins/CoinToCoin.vue').default);
Vue.component('btc-to-coin', require('./components/dashboard/trade-coins/BtcToCoin.vue').default);
Vue.component('coin-to-btc', require('./components/dashboard/trade-coins/CoinToBtc.vue').default);
Vue.component('eth-to-coin', require('./components/dashboard/trade-coins/EthToCoin.vue').default);
Vue.component('coin-to-eth', require('./components/dashboard/trade-coins/CoinToEth.vue').default);
Vue.component('sell-coins', require('./components/dashboard/sell-coins/SellCoins.vue').default);
Vue.component('import-coins', require('./components/dashboard/import-coins/ImportCoins.vue').default);
Vue.component('coinbase-import', require('./components/dashboard/import-coins/CoinbaseImport.vue').default);
Vue.component('binance-import', require('./components/dashboard/import-coins/BinanceImport.vue').default);
Vue.component('gdax-import', require('./components/dashboard/import-coins/GdaxImport.vue').default);
Vue.component('tools', require('./components/dashboard/tools/Tools.vue').default);
Vue.component('average-cost-calculator', require('./components/dashboard/tools/AverageCostCalculator.vue').default);
Vue.component('conversion-calculator', require('./components/dashboard/tools/ConversionCalculator.vue').default);



Vue.component('progress-bar', require('./components/components/Progress.vue').default);

Vue.component('admin-dashboard', require('./components/admin/AdminDashboard.vue').default);
Vue.component('admin-overview', require('./components/admin/AdminOverview.vue').default);
Vue.component('admin-posts', require('./components/admin/AdminPosts.vue').default);
Vue.component('admin-add-post', require('./components/admin/AdminAddPost.vue').default);


/**
 * VUE FILTERS
 */
Vue.filter('number', function(value) {
    return value.replace(/^0+(\d)|(\d)0+$/gm, '$1$2')
});
Vue.filter('coinurl', function(value) {
    return value.replace(/\s+/g, '-').toLowerCase()
});
Vue.filter('formatDate', function(value) {
    return moment(String(value)).format('MM/DD/YY')
});
Vue.filter('formatDateTime', function(value) {
    return moment(String(value)).format('MM/DD/YY h:mma')
});
Vue.filter('percentage', function(value, decimals) {
    if(!value) {
        value = 0;
    }

    if(!decimals) {
        decimals = 0;
    }

    value = value * 100;
    value = Math.round(value * Math.pow(10, decimals)) / Math.pow(10, decimals);
    value = value + '%';
    return value;
});
Vue.filter('reduceNumber', function(value) {
    if (value >= 1000000000){
        return Math.round((value/1000000000) * 100) / 100 + 'B'
    } else if (value >= 1000000) {
        return Math.round((value/1000000) * 100) / 100 + 'M'
    } else if (value >= 1000) {
        return Math.round((value/1000) * 100) / 100 + 'K'
    } else {
        return value
    }
});
const app = new Vue({
    el: '#vue',
});
