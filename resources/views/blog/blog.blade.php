@extends('layouts.app')
@section('content')
    <section class="hero">
        <div class="hero-body">
            <div class="container">
                <div class="columns is-desktop is-centered">
                    <div class="column is-8">
                        <h1 class="title has-text-primary">Guru Blog</h1>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section">
        <div class="container">
            <div class="columns is-tablet is-centered">
                <div class="column is-3 is-2-widescreen">
                    <div class="row">
                        <i class="fa fa-archive" aria-hidden="true"></i>
                        <span class="has-text-weight-bold">
                        Archives
                        </span>
                    </div>
                    <div class="row" style="margin:1em 0;">
                        <ul>
                            @foreach ($archives as $archive)
                                <li><a> {{ $archive->month }} ({{ $archive->posts }})</a></li>
                                @endforeach
                        </ul>
                    </div>
                    <hr>
                    <div class="row">
                        <i class="fa fa-tags" aria-hidden="true"></i>
                        <span class="has-text-weight-bold">
                        Tags
                        </span>
                    </div>
                    <div class="row" style="margin:1em 0;">
                        <div class="columns is-multiline">
                            <div class="column">
                                @foreach ($tags as $tag)
                                    <span class="tag">{{ $tag->slug }}</span>
                                @endforeach
                            </div>

                        </div>
                    </div>

                </div>
                <div class="column is-9 is-8-desktop is-6-widescreen">
                    @foreach ($posts as $post)
                    <div class="card" style="margin-bottom:2rem;">
                        <div class="card-content">
                        <a href="/blog/{{ $post->slug }}">
                        <h1 class="title has-text-primary">
                            <span class="icon">
                                <i class="fa fa-newspaper-o" aria-hidden="true"></i>
                            </span>&nbsp;
                            <span>{{ $post->title }}</span>
                        </h1>
                        </a>
                        <small>By <a>{{ $post->author->name }}</a> on {{ \Carbon\Carbon::parse($post->published_at)->toFormattedDateString() }}</small>
                            <div class="row has-text-centered">
                                <img src="{{ $post->featured_image }}">
                            </div>
                        <div class="m-t-20">
                            {!! blogExcerpt($post->content, $post->slug) !!}
                        </div>
                        </div>
                    </div>
                        @endforeach
                </div>
            </div>
        </div>
    </section>
    @endsection