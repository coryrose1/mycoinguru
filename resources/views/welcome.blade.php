@extends('layouts.app')
@section('content')
    <section class="hero">
        <div class="hero-body" style="padding-top:6rem;padding-bottom:6rem">
            <div class="container">
                <div class="columns is-vcentered">
                    <div class="column is-4 has-text-centered">
                        @darkmode
                        <img src="{{ asset('img/coinguruhero-dark.svg') }}" style="width:90%">
                        @else
                            <img src="{{ asset('img/coinguruhero.svg') }}" style="width:90%">
                            @enddarkmode

                    </div>
                    <div class="column is-8">
                        <h1 class="title is-1">Welcome to My Coin Guru</h1>
                        <h2 class="subtitle is-3">A Smart Portfolio for your Cryptocurrency</h2>
                        @guest('web')
                            <div class="field is-grouped">
                                <p class="control">
                                    <a class="button is-large is-primary" href="{{ route('register') }}">Create Your Portfolio</a>
                                </p>
                                <p class="control">
                                    <a class="button is-large is-info" href="{{ route('login') }}">Sign In</a>
                                </p>
                            </div>
                            @else
                                <a class="button is-large is-primary" href="{{ route('dashboard') }}">View Your Portfolio</a>
                            @endguest
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="hero" style="background-color:whitesmoke;">
        <div class="hero-body" style="padding-top:6rem;padding-bottom:6rem">
        <div class="container">
            <div class="columns">
                <div class="column has-text-centered">
                    <h1 class="title is-size-3">View and manage your Portfolio.</h1>
                    <img src="{{ asset('img/dash1.png') }}">
                </div>
                <div class="column has-text-centered">
                    <h1 class="title is-size-3">Import from Coinbase, GDAX, Binance, & more.</h1>
                    <img src="{{ asset('img/coinbase_oauth.png') }}">
                </div>
                <div class="column has-text-centered">
                    <h1 class="title is-size-3">Explore cryptocurrencies. Featuring Dark Mode.</h1>
                    <img src="{{ asset('img/dash3.png') }}">
                </div>
            </div>
        </div>
        </div>
    </section>
    @endsection