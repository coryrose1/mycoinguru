<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xhtml="http://www.w3.org/1999/xhtml">
    <url>
        <loc>https://mycoinguru.com/</loc>


        <lastmod>2018-01-22T12:46:45+00:00</lastmod>

        <changefreq>daily</changefreq>

        <priority>0.8</priority>
    </url>
    <url>
        <loc>https://mycoinguru.com/blog</loc>


        <lastmod>2018-01-22T12:46:45+00:00</lastmod>

        <changefreq>daily</changefreq>

        <priority>0.8</priority>
    </url>
    <url>
        <loc>https://mycoinguru.com/coins</loc>


        <lastmod>2018-01-22T12:46:45+00:00</lastmod>

        <changefreq>daily</changefreq>

        <priority>0.8</priority>
    </url>
    <url>
        <loc>https://mycoinguru.com/help</loc>


        <lastmod>2018-01-22T12:46:45+00:00</lastmod>

        <changefreq>daily</changefreq>

        <priority>0.8</priority>
    </url>
    <url>
        <loc>https://mycoinguru.com/login</loc>


        <lastmod>2018-01-22T12:46:45+00:00</lastmod>

        <changefreq>daily</changefreq>

        <priority>0.8</priority>
    </url>
    <url>
        <loc>https://mycoinguru.com/password/reset</loc>


        <lastmod>2018-01-22T12:46:45+00:00</lastmod>

        <changefreq>daily</changefreq>

        <priority>0.8</priority>
    </url>
    <url>
        <loc>https://mycoinguru.com/register</loc>


        <lastmod>2018-01-22T12:46:45+00:00</lastmod>

        <changefreq>daily</changefreq>

        <priority>0.8</priority>
    </url>

    <url>
        <loc>https://mycoinguru.com/changelog</loc>


        <lastmod>2018-01-22T12:46:45+00:00</lastmod>

        <changefreq>daily</changefreq>

        <priority>0.8</priority>
    </url>
    @foreach ($posts as $post)
        <url>
            <loc>https://mycoinguru.com.com/{{ $post->slug }}</loc>
            <lastmod>{{ $post->published_at->tz('UTC')->toAtomString() }}</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.6</priority>
        </url>
    @endforeach
</urlset>