@extends('layouts.app')
@section('content')
   @guest
   <coins></coins>
   @else
      <coins auth="{{ \Illuminate\Support\Facades\Auth::user()->id }}"></coins>
   @endguest
    @endsection