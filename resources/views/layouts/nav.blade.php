<nav class="navbar has-shadow" role="navigation" aria-label="main navigation">
    <div class="container">
    <div class="navbar-brand">
        <a class="navbar-item has-text-weight-bold" href="/">
            <img src="{{ asset('img/guru.svg') }}" style="width:26px;height:36px;max-height:none;">&nbsp;My Coin Guru
        </a>
        <a class="navbar-item is-hidden-desktop" href="https://join.slack.com/t/mycoinguru/shared_invite/enQtMjkzNTQ1NjI1MjE4LWMxNGFhZGRhOWZjMWM0YzJhYzg5OWRlZTZjYTcxNmM4MmMzNjYwYWYwM2I3NmEyOGZmZjU1ZGExMTc3NjY5ODk">
                <span class="icon">
                    <i class="fa fa-slack"></i>
                </span>
        </a>

        <button class="button navbar-burger" data-target="navMenu">
            <span></span>
            <span></span>
            <span></span>
        </button>
    </div>
    <div class="navbar-menu" id="navMenu">
        <div class="navbar-start">
                <a href="{{ route('dashboard') }}" class="navbar-item is-tab {{ Nav::hasSegment('dashboard', 1) }}">Dashboard</a>
            <a href="{{ route('coins.index') }}" class="navbar-item is-tab {{ Nav::hasSegment('coins', 1) }}">Coins</a>
            <a href="/changelog" class="navbar-item is-tab {{ Nav::hasSegment('changelog', 1) }}">Changelog</a>
        </div>
        <div class="navbar-end">
            <a class="navbar-item is-hidden-touch" href="https://join.slack.com/t/mycoinguru/shared_invite/enQtMjkzNTQ1NjI1MjE4LWMxNGFhZGRhOWZjMWM0YzJhYzg5OWRlZTZjYTcxNmM4MmMzNjYwYWYwM2I3NmEyOGZmZjU1ZGExMTc3NjY5ODk">
                <span class="icon" style="vertical-align: middle">
                                    <i class="fa fa-slack"></i>
                                </span>
            </a>
            <div class="navbar-item has-dropdown is-hoverable">
                <a class="navbar-link">Help</a>
                <div class="navbar-dropdown">
                    <a href="/help" class="navbar-item">Help Guide</a>
                    <a href="mailto:coryrosenwald@gmail.com?subject=My Coin Guru Question/Feedback" class="navbar-item">Questions/Feedback</a>
                </div>
            </div>
            @guest
                <a href="{{ route('login') }}" class="navbar-item">Login</a>
                <a href="{{ route('register') }}" class="navbar-item">Sign Up</a>
                @else
                    <div class="navbar-item has-dropdown is-hoverable">
                        <a class="navbar-link">
                            @if (Auth::user()->subscribed('Monthly') || Auth::user()->subscribed('Yearly'))
                                <i class="fa fa-certificate" aria-hidden="true" style="color:goldenrod"></i>&nbsp;{{ Auth::user()->name }}
                                @else
                                {{ Auth::user()->name }}
                                @endif

                        </a>

                        <div class="navbar-dropdown">
                            <a class="navbar-item" href="{{ route('settings') }}">Account Settings</a>
                            @writer
                            <a class="navbar-item" href="{{ route('admin.dashboard') }}">Admin Dashboard</a>
                            @endwriter
                            @if (Auth::user()->dark_mode == 0)
                            <a class="navbar-item" href="{{ route('dark-mode') }}">Dark Mode</a>
                            @else
                                <a class="navbar-item" href="{{ route('dark-mode') }}">Light Mode</a>
                                @endif
                            <a class="navbar-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
                        </div>
                    </div>
                @endif
        </div>
    </div>
    </div>
</nav>

@push('javascript')
    document.addEventListener('DOMContentLoaded', function () {

    // Get all "navbar-burger" elements
    var $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

    // Check if there are any navbar burgers
    if ($navbarBurgers.length > 0) {

    // Add a click event on each of them
    $navbarBurgers.forEach(function ($el) {
    $el.addEventListener('click', function () {

    // Get the target from the "data-target" attribute
    var target = $el.dataset.target;
    var $target = document.getElementById(target);

    // Toggle the class on both the "navbar-burger" and the "navbar-menu"
    $el.classList.toggle('is-active');
    $target.classList.toggle('is-active');

    });
    });
    }

    });
    @endpush