<footer class="footer">
    <div class="container">
        <div class="columns is-vcentered">
            <div class="column has-text-centered">
                <img src="{{ asset('img/thecoinguru.png') }}" style="height:120px">
            </div>
            <div class="column has-text-centered">
                <p>
                    <strong>My Coin Guru</strong> by <a href="https://coryrose.com" target="_blank" rel="noopener">Cory Rosenwald</a>
                    <br>
                    Currency data provided by <a href="https://coinmarketcap.com" target="_blank" rel="noopener">CoinMarketCap.com</a>
                </p>
            </div>
            <div class="column has-text-centered">
                <donate-eth-modal></donate-eth-modal>
            </div>
        </div>
    </div>
</footer>