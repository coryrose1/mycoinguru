<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/cryptocoins.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        @include('layouts.nav')

        <div class="container">
            <div class="row">
                <div class="columns">
                    <div class="column is-2">
                        <aside class="menu">
                            <p class="menu-label">
                                General
                            </p>
                            <ul class="menu-list">
                                <li><a class="{{ Nav::isRoute('dashboard') }}" href="{{ route('dashboard') }}">Home</a></li>
                                <li><a class="{{ Nav::hasSegment('my-coins', 2) }}" href="{{ route('my-coins') }}">My Coins</a></li>
                            </ul>
                        </aside>
                    </div>
                    <div class="column is-10">
                        @yield('content')
                    </div>
                </div>
            </div>
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    {{--<script type="text/javascript">--}}
    {{--@stack('javascript')--}}
        {{--$( document ).ready(function() {--}}
            {{--@stack('jquery')--}}
        {{--});--}}
    {{--</script>--}}
</body>
</html>
