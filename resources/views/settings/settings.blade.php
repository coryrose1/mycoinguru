@extends('layouts.app')
@section('content')
    <section class="hero">
        <div class="hero-body">
            <div class="container">
                <h1 class="title">
                    Account Settings
                </h1>
            </div>
        </div>
    </section>
    <div class="container">
        <div class="columns is-desktop">
            <div class="column is-2-desktop">
                <aside class="menu">
                    <p class="menu-label">
                        Settings
                    </p>
                    <ul class="menu-list">
                        <li><a href="{{ route('settings') }}" class="{{ Nav::isRoute('settings') }}">
                                <span class="icon" style="vertical-align: middle">
                                    <i class="fa fa-cogs"></i>
                                </span>
                                <span>General</span>
                            </a></li>
                        <li><a href="{{ route('settings.api') }}" class="{{ Nav::isRoute('settings.api') }}">
                                <span class="icon" style="vertical-align: middle">
                                    <i class="fa fa-key"></i>
                                </span>
                                <span>API Access</span>
                                </a></li>
                    </ul>
                </aside>
            </div>
            <div class="column is-10-desktop">
                @yield('settings')
            </div>
        </div>
    </div>
    @endsection