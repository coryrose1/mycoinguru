@extends('settings.settings')
@section('settings')
    <section>
        <div class="columns is-vcentered">
            <div class="column is-4-desktop">
                <h1 class="title">Purge Account</h1>
                <h3 class="subtitle">Purge account of all buys, sells, trades, and imports. API keys will remain.</h3>
            </div>
            <div class="column is-8-desktop has-text-right">
                <a class="button is-danger" disabled>Coming Soon</a>
            </div>
        </div>
    </section>
    <hr class="m-t-40 m-b-40">
    @endsection