@extends('settings.settings')
@section('settings')
    <section>
        <div class="columns">
            <div class="column">
                <h1 class="title">Coinbase API</h1>
                <h3 class="subtitle">Authenticate with Coinbase via OAuth (read-only).</h3>
                <img src="/img/coinbase.png">
            </div>
            <div class="column is-6-desktop has-text-centered">
                @if ($user->coinbaseApi)
                    <div class="notification">
                        <div class="row has-text-centered">
                            <span class="has-text-weight-bold">Coinbase Access Token:</span>
                        </div>
                        <div class="row m-t-15 has-text-centered">
                            <pre>{{ $user->coinbaseApi->access_token }}</pre>
                        </div>
                        <div class="row m-t-20">
                            <div class="columns">
                                <div class="column has-text-left"><small><span class="has-text-weight-bold">Added:</span>&nbsp;{{ $user->coinbaseApi->created_at->format('m/d/y') }}</small></div>
                                <div class="column has-text-right">
                                    <a class="button is-danger" href="{{ route('api.coinbase.destroy', $user->coinbaseApi->id) }}">Delete</a>
                                </div>
                            </div>
                        </div>
                    </div>
                @else
                    <a class="button is-info" href="{{ route('coinbase.oauth') }}">Connect to Coinbase</a>
                @endif
            </div>
        </div>
    </section>
    <hr class="m-t-40 m-b-40">
    <section>
        <div class="columns">
            <div class="column">
                <h1 class="title">GDAX API</h1>
                <h3 class="subtitle">GDAX API credentials.</h3>
                <img src="/img/gdax.png">
            </div>
            <div class="column is-6-desktop">
                @if ($user->gdaxApi)
                <div class="notification">
                    <div class="row has-text-centered">
                        <span class="has-text-weight-bold">GDAX API Key:</span>
                    </div>
                    <div class="row m-t-15 has-text-centered">
                        <pre>{{ $user->gdaxApi->api_key }}</pre>
                    </div>
                    <div class="row m-t-20">
                        <div class="columns">
                            <div class="column has-text-left"><small><span class="has-text-weight-bold">Added:</span>&nbsp;{{ $user->gdaxApi->created_at->format('m/d/y') }}</small></div>
                            <div class="column has-text-right">
                                <a class="button is-danger" href="{{ route('api.gdax.destroy', $user->gdaxApi->id) }}">Delete</a>
                            </div>
                        </div>
                    </div>
                </div>
                @else
                <div class="notification">
                    <form class="form" method="POST" action="/api/gdax">
                        {{ csrf_field() }}
                        <div class="field">
                            <label class="label">Passphrase</label>
                            <div class="control">
                                <input class="input" type="text" name="passphrase">
                            </div>
                        </div>
                        <div class="field">
                            <label class="label">API Key</label>
                            <div class="control">
                                <input class="input" type="text" name="key">
                            </div>
                        </div>
                        <div class="field">
                            <label class="label">API Secret</label>
                            <div class="control">
                                <input class="input" type="text" name="secret">
                            </div>
                        </div>
                        <div class="field">
                            <button class="button is-primary">Save</button>
                        </div>
                    </form>
                </div>
                    @endif
            </div>
        </div>
    </section>
    <hr class="m-t-40 m-b-40">
    <section>
        <div class="columns">
            <div class="column">
                <h1 class="title">Binance API</h1>
                <h3 class="subtitle">Run this import to review and import trades from Binance.</h3>
                <img src="/img/binance.png">
            </div>
            <div class="column is-6-desktop">
                @if ($user->binanceApi)
                <div class="notification">
                    <div class="row has-text-centered">
                        <span class="has-text-weight-bold">Binance API Key:</span>
                    </div>
                    <div class="row m-t-15 has-text-centered">
                        <pre>{{ $user->binanceApi->api_key }}</pre>
                    </div>
                    <div class="row m-t-20">
                        <div class="columns">
                            <div class="column has-text-left"><small><span class="has-text-weight-bold">Added:</span>&nbsp;{{ $user->binanceApi->created_at->format('m/d/y') }}</small></div>
                            <div class="column has-text-right">
                                <a class="button is-danger" href="{{ route('api.binance.destroy', $user->binanceApi->id) }}">Delete</a>
                            </div>
                        </div>
                    </div>
                </div>
                @else
                <div class="notification">
                    <form class="form" method="POST" action="/api/binance">
                        {{ csrf_field() }}
                        <div class="field">
                            <label class="label">API Key</label>
                            <div class="control">
                                <input class="input" type="text" name="key">
                            </div>
                        </div>
                        <div class="field">
                            <label class="label">API Secret</label>
                            <div class="control">
                                <input class="input" type="text" name="secret">
                            </div>
                        </div>
                        <div class="field">
                            <button class="button is-primary">Save</button>
                        </div>
                    </form>
                </div>
                    @endif
            </div>
        </div>
    </section>
    <hr class="m-t-40 m-b-40">
    <section>
        <div class="columns">
            <div class="column">
                <h1 class="title">Bittrex API</h1>
                <h3 class="subtitle">Run this import to review and import trades from Binance.</h3>
                <img src="/img/bittrex.png">
            </div>
            <div class="column has-text-centered">
                <h3 class="is-size-3">In Development</h3>
            </div>
        </div>
    </section>
    <hr class="m-t-40 m-b-40">
    @endsection