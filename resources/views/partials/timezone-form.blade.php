<div class="field">
    <label class="label">Select Timezone</label>
    <div class="select">
        <select name="timezone">
            <option value="America/New_York">America/New_York</option>
            <option value="America/Chicago">America/Chicago</option>
            <option value="America/Denver">America/Denver</option>
            <option value="America/Phoenix">America/Phoenix</option>
            <option value="America/Los_Angeles">America/Los_Angeles</option>
            <option value="America/Anchorage">America/Anchorage</option>
            <option value="America/Adak">America/Adak</option>
            <option value="Pacific/Honolulu">Pacific/Honolulu</option>
        </select>
    </div>
</div>