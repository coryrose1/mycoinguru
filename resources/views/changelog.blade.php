@extends('layouts.app')
@section('content')
    <section class="hero">
        <div class="hero-body p-b-10">
            <div class="container">
                <h1 class="title">
                    Changelog
                </h1>
            </div>
        </div>
    </section>
    <section class="section p-t-10">
        <div class="container">
            <div class="content">
                <hr>
                <h4 class="is-size-4">
                    3/30/18
                </h4>
                <ul>
                    <li>Created <a href="/dashboard#tools">Tools</a> section and added Average Cost Calculator.</li>
                </ul>
                <hr>
                <h4 class="is-size-4">
                    3/24/18
                </h4>
                <ul>
                    <li>Candlestick graphs have been added to coin detail sheet.</li>
                    <li>'Hide <$1 Holdings' option added to Portfolio.</li>
                </ul>
                <hr>
                <h4 class="is-size-4">
                    3/12/18
                </h4>
                <ul>
                    <li>Missed a few updates since January but a lot has been done:</li>
                    <li>Many general improvements in pages such as Coin Detail & Transactions.</li>
                    <li>Exchange imports have been greatly improved and now tie directly to your transactions, so the Guru knows what has been imported, and will mark transactions as not imported if they are deleted.</li>
                    <li>Icons have been improved courtesy of <a href="https://bitonics.net/" target="_blank" rel="noopener">https://bitonics.net/</a>.</li>
                    <li>Premium features are being introduced such as automated/daily exchange imports, text alerts, and more - coming soon.</li>
                </ul>
                <hr>
                <h4 class="is-size-4">
                    1/13/18
                </h4>
                <ul>
                    <li>The select forms have been improved on Add Coins, Trade Coins, and Sell Coins pages to allow for easier coin selection.</li>
                    <li>The Transactions page in your <a href="/dashboard">Portfolio</a> has been revised and improved and displays all purchases, trades, and sales.</li>
                </ul>
                <hr>
                <h4 class="is-size-4">
                    1/6/18
                </h4>
                <ul>
                    <li>Began implementation of the <a href="/help">My Coin Guru Help Guide</a></li>
                    <li>Reference this guide to learn how My Coin Guru works, how to import and manage your crypto holdings, and more!</li>
                </ul>
                <hr>
                <h4 class="is-size-4">
                    1/3/18
                </h4>
                <ul>
                    <li>GDAX and Binance imports now available via the <a href="/dashboard#import-coins">Import Coins page</a></li>
                    <ul>
                        <li>For GDAX, create a READ ONLY API in your GDAX account settings, and add the keys in your API page in your My Coin Guru <a href="/settings">settings page.</a></li>
                        <li>For Binance, export the "Order History" report and use the import on the <a href="/dashboard#import-coins">Import Coins page</a></li>
                    </ul>
                    <li>Fullscreen portfolio table has been added. Click the fullscreen button when viewing the normal table.</li>
                </ul>
                <hr>
                <h4 class="is-size-4">
                    1/1/18
                </h4>
                <ul>
                    <li>You may now import your buys and sells straight from your Coinbase account via the <a href="/dashboard#import-coins">Import Coins page</a> in your Dashboard.</li>
                    <li>More import functionality is coming soon - including importing your trades from Binance and Bittrex.</li>
                    <li>1hr, 24hr, and 7d % change added to portfolio sort.</li>
                </ul>
                <hr>
                <h4 class="is-size-4">
                    12/29/17
                </h4>
                <ul>
                    <li>Detailed overview cards have been changed to a sortable table. You can save this as your default view by setting the switch to Table and clicking the save icon.</li>
                    <li>A <a href="mailto:coryrosenwald@gmail.com?subject=My Coin Guru Question/Feedback">Questions/Feedback</a> link has been added to e-mail me with any questions/feedback. Please feel free to reach out.</li>
                </ul>
                <hr>
                <h4 class="is-size-4">
                    12/24/17
                </h4>
                <ul>
                    <li>The Watchlist / Coin Ticker on all pages is now customizable - add your favorite holds or prospects to your Watchlist by clicking the Edit button under the navigation bar!</li>
                </ul>
                <hr>
                <h4 class="is-size-4">
                    12/17/17
                </h4>
                <ul>
                    <li>ETH <i class="fa fa-long-arrow-right"></i> Coin & Coin <i class="fa fa-long-arrow-right"></i> ETH trading has been implemented.</li>
                    <li>Both BTC and ETH trades now utilize the <a href="https://docs.gdax.com/#get-historic-rates" target="_blank" rel="noopener">GDAX API</a> in order to determine price of coin at trade.</li>
                </ul>
                <hr>
                <h4 class="is-size-4">
                    12/10/17
                </h4>
                <ul>
                    <li>Added Sales functionality - you may now <a href="/dashboard#sell-coins">sell coins for USD</a>.</li>
                    <ul>
                        <li>Note that this will subtract the amount of sold coin at your average cost value from your Portfolio.</li>
                        <li>You may use the <a href="/dashboard#sales">My Sales page</a> to track your historical sales, and individual and total profits.</li>
                    </ul>
                </ul>
                <hr>
                <h4 class="is-size-4">
                    12/8/17
                </h4>
                <ul>
                    <li>Implemented <a href="{{ route('dark-mode') }}">Dark Mode</a>, which can be toggled from your account dropdown in the navigation.</li>
                </ul>
                <hr>
                <h4 class="is-size-4">
                    12/4/17
                </h4>
                <ul>
                    <li>The Overview cards in your <a href="/dashboard">Portfolio</a> are now expandable.</li>
                    <li>Investment, Profit, and ROI sorting options added to Overview.</li>
                    <li>You may saved your preferred sort in the dashboard.</li>
                </ul>
            <hr>
            <h4 class="is-size-4">
                12/3/17
            </h4>
            <ul>
                <li>BTC <i class="fa fa-long-arrow-right"></i> Coin & Coin <i class="fa fa-long-arrow-right"></i> BTC trading has been implemented.</li>
            </ul>
            <ul>
                <li>Click the detail button(s) in your <a href="/dashboard">Portfolio</a> to show more information for your individual coin investments.</li>
                <p>This includes data like:</p>
                <ul>
                    <li>Individual investments</li>
                    <li>Average Cost</li>
                    <li>Coin ROI</li>
                </ul>
            </ul>
            <ul>
                <li>A visual breakdown for your investments has been implemented in the <a href="/dashboard">Portfolio</a>. It features pie, doughnut, and bar charts for your investment revenue, ROI, and volume by coin.</li>
            </ul>
            <ul>
                <li>The default portfolio is now sortable.</li>
            </ul>
            <hr>
            <h4 class="is-size-4">
                11/27/17
            </h4>
            <ul>
                <li>The number sorting on the <a href="/coins">Coins</a> table has been fixed.</li>
                <li>The coin tickers at top center of page now link to their respective <a href="https://coinmarketcap.com">Coin Market Cap</a> page.</li>
                <li>This changelog has been implemented.</li>
            </ul>
            </div>
        </div>
    </section>
    @endsection