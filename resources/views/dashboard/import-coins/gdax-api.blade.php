@extends('layouts.app')
@section('content')
    <section class="hero">
        <div class="hero-body">
            <div class="container">
                <div class="row" style="display:flex;justify-content:space-between">
                    <h1 class="title">
                        GDAX API
                    </h1>
                    <img src="/img/gdax.png" style="height:60px">
                </div>
            </div>
        </div>
    </section>
    @endsection