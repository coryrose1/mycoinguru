@extends('layouts.dashboard')
@section('title')
    My Coins
    @endsection
@section('content')
    <div class="row m-b-30">
        @include('dashboard.add-coin')
    </div>
    <div class="row">
        @if (count($user->coins))
            <table class="table">
                <thead>
                    <tr>
                        <th>Coin</th>
                        <th>Amount</th>
                        <th>Price</th>
                        <th>Added</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($user->coins as $coin)
                        <tr>
                            <td>{{$coin->coin->symbol}}</td>
                            <td>{{$coin->amount}}</td>
                            <td>{{$coin->price}}</td>
                            <td>{{$coin->created_at}}</td>
                            <td>Action</td>
                        </tr>
                        @endforeach
                </tbody>
            </table>
            @endif
    </div>
    @endsection