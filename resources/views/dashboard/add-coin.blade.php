<form method="POST" action="{{ route('add-coin') }}">
    {{ csrf_field() }}
    <div class="field has-addons has-addons-centered">
        <p class="control">
            <input class="input" type="number" step="any" name="amount" placeholder="Amount of coin">
            @if ($errors->has('amount'))
                {{ $errors->first('amount') }}
            @endif
        </p>
        <p class="control">
    <span class="select">
      <select name="coin">
        @foreach ($coins as $coin)
            <option value="{{$coin->id}}">{{$coin->symbol}} | {{ $coin->name }}</option>
            @endforeach
      </select>
        @if ($errors->has('coin'))
            {{ $errors->first('coin') }}
            @endif
    </span>
        </p>
        <p class="control">
            <input class="input" type="number" step="any" name="price" placeholder="Price purchased at">
            @if ($errors->has('price'))
                {{ $errors->first('price') }}
            @endif
        </p>
        <p class="control">
            <button class="button is-primary" type="submit">Add</button>
        </p>
    </div>
</form>