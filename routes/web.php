<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Controller@index')->name('welcome');
Route::feeds();
Auth::routes();

Route::prefix('blog')->group(function () {
   Route::get('/', 'BlogController@index');
});
Route::prefix('dashboard')->group(function () {
    Route::get('/', 'DashboardController@index')->name('dashboard');
    Route::get('/stats', 'DashboardController@stats')->name('dashboard.stats');
    Route::get('/breakdown', 'DashboardController@breakdown')->name('dashboard.breakdown');
    Route::get('/history', 'DashboardController@history')->name('dashboard.history');
    Route::get('/detail/{symbol}', 'DashboardController@coinDetail')->name('dashboard.coin-detail');

    Route::get('/holdings/by-coin', 'DashboardController@byCoin')->name('holdings.by-coin');
    Route::get('/holdings/by-purchase', 'DashboardController@byPurchase')->name('holdings.by-purchase');

    Route::post('/add/usd-to-coin', 'TransactionsController@store')->name('transactions.store');
    Route::get('/transactions/all', 'DashboardController@getAllTransactions');
    Route::get('/transactions/delete/{id}', 'TransactionsController@destroy')->name('transactions.destroy');

    Route::post('/trade/btc-to-coin', 'TradesController@btcToCoin');
    Route::post('/trade/coin-to-btc', 'TradesController@coinToBtc');
    Route::post('/trade/eth-to-coin', 'TradesController@ethToCoin');
    Route::post('/trade/coin-to-eth', 'TradesController@coinToEth');
    Route::get('/get-trades', 'TradesController@getTrades');
    Route::get('/trades/delete/{id}', 'TradesController@destroy')->name('trades.destroy');

    Route::post('/sell-coins', 'SalesController@store');
    Route::get('/sales/all', 'SalesController@getSales');
    Route::get('/sales/delete/{id}', 'SalesController@destroy')->name('sales.destroy');

    Route::get('/sort', 'UsersSortController@getSort');
    Route::post('/sort', 'UsersSortController@store');

    Route::get('/watchlist', 'WatchlistController@getWatchlist');
    Route::post('/watchlist', 'WatchlistController@store')->name('watchlist.store');
    Route::get('/watchlist/delete/{id}', 'WatchlistController@destroy')->name('watchlist.destroy');
    Route::get('/watchlist/available-coins', 'WatchlistController@availableCoins');

    Route::get('/dark-mode', 'DashboardController@darkMode')->name('dark-mode');

    Route::get('/is-admin', 'DashboardController@isAdmin');

    Route::post('/hide-low-holdings', 'DashboardController@hideLowHoldings');

});

Route::prefix('/settings')->group(function() {
    Route::get('/', 'SettingsController@index')->name('settings');
    Route::get('/api', 'SettingsController@api')->name('settings.api');
    Route::view('/subscription', 'subscription');
    Route::post('/subscription/create', 'SubscriptionsController@store');
});

Route::prefix('admin')->middleware(\App\Http\Middleware\IsWriter::class)->group(function() {
    Route::view('/', 'admin.admin')->name('admin.dashboard');
    Route::post('/add-post', 'BlogController@store');
    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
});


Route::get('/coins/get', 'CoinsController@get');
Route::get('/coins/get/symbols', 'CoinsController@getSymbols');
Route::resource('coins', 'CoinsController');
Route::view('/help', 'help')->name('help');
Route::get('/ticker', 'Controller@getTicker');
Route::get('/test', 'TestController@index')->name('test');
Route::get('/cryptocompare/{coin}/{priceIn}', 'ExternalApiController@cryptoCompare');

Route::get('/updateCoins', 'CoinsController@updatePrices');
Route::get('/importCoins', 'CoinsController@importCoins');
Route::view('/changelog', 'changelog');

Route::post('/guru-bot', 'SlackController@guruQuote');

Route::get('/api/all', 'UsersApiController@getApis');
Route::get('/api/gdax', 'UsersApiController@getGdax');
Route::post('/api/gdax', 'UsersApiController@storeGdax')->name('api.gdax.store');
Route::get('/api/gdax/delete/{id}', 'UsersApiController@destroyGdax')->name('api.gdax.destroy');
Route::get('/api/binance', 'UsersApiController@getBinance');
Route::post('/api/binance', 'UsersApiController@storeBinance')->name('api.binance.store');
Route::get('/api/binance/delete/{id}', 'UsersApiController@destroyBinance')->name('api.binance.destroy');
Route::get('/api/bittrex', 'UsersApiController@getBittrex');
Route::post('/api/bittrex', 'UsersApiController@storeBittrex');
Route::get('/api/bittrex/delete/{id}', 'UsersApiController@destroyBittrex');
Route::get('/api/coinbase/delete{id}', 'UsersApiController@destroyCoinbase')->name('api.coinbase.destroy');



Route::get('/coinbase/oauth', 'CoinbaseImportController@redirectToCoinbase')->name('coinbase.oauth');
Route::get('/coinbase/refresh', 'CoinbaseImportController@refreshToken')->name('coinbase.oauth.refresh');
Route::get('/coinbase/callback', 'CoinbaseImportController@handleCoinbaseCallback');
Route::get('/coinbase/query', 'CoinbaseImportController@queryCoinbase')->name('coinbase.requery');
Route::get('/coinbase/retrieve-import', 'CoinbaseImportController@retrieveImport');
Route::post('/coinbase/import', 'CoinbaseImportController@handleImport');
Route::view('/coinbase/test', 'dashboard.import-coins.coinbase-import');
Route::get('/coinbase/already-imported', 'CoinbaseImportController@retrieveImported');
Route::post('/coinbase/delete-previous', 'CoinbaseImportController@deletePrevious');
Route::get('/coinbase/missing-transactions', 'CoinbaseImportController@retrieveMissingTransactions');

Route::get('/gdax/fetch', 'GdaxImportController@getFromApi');
Route::get('/gdax/table', 'GdaxImportController@table');
Route::view('/gdax/show-import', 'dashboard.import-coins.gdax-import');
Route::post('/gdax/handle-import', 'GdaxImportController@handleImport');
Route::post('/gdax/mark-as-imported', 'GdaxImportController@markAsImported');
Route::get('/gdax/already-imported', 'GdaxImportController@retrieveImported');
Route::post('/gdax/delete-previous', 'GdaxImportController@deletePrevious');
Route::get('/gdax/missing-transactions', 'GdaxImportController@retrieveMissingTransactions');


Route::view('/binance', 'dashboard.import-coins.binance-import')->name('binance');
Route::post('/binance/import', 'BinanceImportController@importFromExcel');
Route::get('/binance/fetch', 'BinanceImportController@getFromApi');
Route::get('/binance/retrieve-import', 'BinanceImportController@retrieveImport');
Route::post('/binance/handle-import', 'BinanceImportController@handleImport');
Route::get('/binance/missing-trades', 'BinanceImportController@retrieveMissingTrades');

Route::get('/fixmissinggdax', 'GdaxImportController@retrieveMissingIds');

Route::get('/sitemap.xml', 'Controller@sitemap');